﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient

Public Class database_import
    Inherits System.Web.UI.Page

    Dim username As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("username")

        


    End Sub

    Protected Sub parseExcel(ByVal PathFile As String)
        Dim connString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathFile + ";Extended Properties=Excel 12.0"

        ' Create the connection object
        Dim oledbConn As OleDbConnection = New OleDbConnection(connString)
        Try

            ' Open connection
            oledbConn.Open()

            ' Create OleDbCommand object and select data from worksheet Sheet1
            Dim cmd As OleDbCommand = New OleDbCommand("SELECT * FROM [Foglio1$]", oledbConn)

            ' Create new OleDbDataAdapter
            Dim oleda As OleDbDataAdapter = New OleDbDataAdapter()

            oleda.SelectCommand = cmd

            ' Create a DataSet which will hold the data extracted from the worksheet.
            Dim ds As DataSet = New DataSet()

            ' Fill the DataSet from the data extracted from the worksheet.
            oleda.Fill(ds, "name")

            Using mySqlConnection As New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")
                Dim row As DataRow
                Dim count As Integer = 0

                mySqlConnection.Open()

                For Each row In ds.Tables(0).Rows

                    count = count + 1

                    Dim queryString As String = "INSERT INTO customers(name,surname,email,cell,address,father) VALUES(@NOME,@COGNOME,@EMAIL,@CELL,@INDIRIZZO,@FATHER)"

                    Dim command As New SqlCommand(queryString, mySqlConnection)



                    command.Parameters.AddWithValue("@NOME", row(0).ToString)
                    command.Parameters.AddWithValue("@COGNOME", row(1).ToString)
                    command.Parameters.AddWithValue("@EMAIL", row(2).ToString)
                    command.Parameters.AddWithValue("@CELL", row(3).ToString)
                    command.Parameters.AddWithValue("@INDIRIZZO", row(4).ToString)
                    command.Parameters.AddWithValue("@FATHER", username)
                    Try
                        command.ExecuteNonQuery()

                        Button2.Visible = True
                    Catch ex As Exception
                        statusLabel.Text = ex.Message
                    End Try
                Next
            End Using


            


            ' statusLabel.Text = "numero contatti importati -> " + count.ToString

        Catch ex As Exception
            statusLabel.Text = ex.Message
        Finally
            ' Close connection
            oledbConn.Close()
        End Try
    End Sub



    Protected Sub UploadButton_Click()
        If FileUpload1.HasFile Then
            Try
                Dim filename As String = Path.GetFileName(FileUpload1.FileName)
                FileUpload1.SaveAs(Server.MapPath("~/import_file/") + filename)
                statusLabel.Text = "Upload status: File uploaded!"
                parseExcel(Server.MapPath("~/import_file/") + filename)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("dashboard.aspx?username=" + username)
    End Sub
End Class