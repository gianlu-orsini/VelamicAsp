﻿Imports System.Data.SqlClient

Public Class creaCampagna
    Inherits System.Web.UI.Page

    Dim username As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("username")

        If Not IsPostBack Then
            RadioMaschio.Enabled = False
            RadioFemmina.Enabled = False
            TextNome.Enabled = False
        End If
    End Sub

    Protected Sub RadioSMS_CheckedChanged(sender As Object, e As EventArgs) Handles RadioSMS.CheckedChanged
        If RadioSMS.Checked Then
            RadioPASS.Checked = False
            RadioMAIL.Checked = False
        End If
    End Sub

    Protected Sub RadioMAIL_CheckedChanged(sender As Object, e As EventArgs) Handles RadioMAIL.CheckedChanged
        If RadioMAIL.Checked Then
            RadioSMS.Checked = False
            RadioPASS.Checked = False
        End If
    End Sub

    Protected Sub RadioPASS_CheckedChanged(sender As Object, e As EventArgs) Handles RadioPASS.CheckedChanged
        If RadioPASS.Checked Then
            RadioSMS.Checked = False
            RadioMAIL.Checked = False
        End If
    End Sub

    Protected Sub CheckSesso_CheckedChanged(sender As Object, e As EventArgs) Handles CheckSesso.CheckedChanged
        If CheckSesso.Checked Then
            RadioMaschio.Enabled = True
            RadioFemmina.Enabled = True
        Else
            RadioMaschio.Enabled = False
            RadioFemmina.Enabled = False
        End If

    End Sub

    Protected Sub CheckNome_CheckedChanged(sender As Object, e As EventArgs) Handles CheckNome.CheckedChanged
        If CheckNome.Checked Then
            TextNome.Enabled = True
        Else
            TextNome.Enabled = False
        End If
    End Sub

    Protected Sub RadioMaschio_CheckedChanged(sender As Object, e As EventArgs) Handles RadioMaschio.CheckedChanged
        If RadioMaschio.Checked Then
            RadioFemmina.Checked = False
        End If
    End Sub

    Protected Sub RadioFemmina_CheckedChanged(sender As Object, e As EventArgs) Handles RadioFemmina.CheckedChanged
        If RadioFemmina.Checked Then
            RadioMaschio.Checked = False
        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        
        Dim queryString As String = "SELECT * from customers WHERE father like @ID " + CalcolaStringaFiltri()


        Using mySqlConnection As New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")
            Dim command As New SqlCommand(queryString, mySqlConnection)
            Dim DataSet1 As New DataSet
            command.Parameters.Add("@ID", SqlDbType.Text)
            command.Parameters("@ID").Value = username

            mySqlConnection.Open()

            Dim MonAdapteur As SqlDataAdapter = New SqlDataAdapter(command)
            MonAdapteur.Fill(DataSet1)
            command.Dispose()

            If DataSet1.Tables.Count > 0 Then
                Dim table As New DataTable
                table = DataSet1.Tables(0)
                lblNumeroContatti.Text = "Numero contatti Che soddisfano i filtri : " + table.Rows.Count.ToString
                GridView1.DataSource = table
                GridView1.DataBind()
            End If
        End Using
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Response.Redirect("dashboard.aspx?username=" + username)
    End Sub

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        

        Dim queryString As String = "INSERT INTO CAMPAGNE(father,query,datacreazione,numeroInvii,name,type) VALUES(@FATHER,@QUERY,@DATACREAZ,@NUMINVII,@NAME,@TYPE)"

        Using mySqlConnection As New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")
            Dim command As New SqlCommand(queryString, mySqlConnection)

            mySqlConnection.Open()

            command.Parameters.AddWithValue("@QUERY", CalcolaStringaFiltri())
            command.Parameters.AddWithValue("@FATHER", username)
            command.Parameters.AddWithValue("@DATACREAZ", Date.Now)
            command.Parameters.AddWithValue("@NUMINVII", 0)
            command.Parameters.AddWithValue("@NAME", textName.text)
            command.Parameters.AddWithValue("@TYPE", calcolaTipoCampagna())
            Try
                command.ExecuteNonQuery()
                lblNumeroContatti.Text = "Campagna Salvata Correttamente"
                Button3.Enabled = False
                Button2.Visible = True
            Catch ex As Exception
                lblNumeroContatti.Text = ex.Message
            End Try


        End Using

    End Sub

    Protected Function CalcolaStringaFiltri() As String
        Dim filtriQuery As String = ""

        If RadioMAIL.Checked Then
            filtriQuery = filtriQuery + " and ( datalength(email)>0 ) "
        End If
        If RadioPASS.Checked Then
            filtriQuery = filtriQuery + " and ( datalength(email)>0 or datalength(cell)>8 )"
        End If
        If RadioSMS.Checked Then
            filtriQuery = filtriQuery + " and ( datalength(cell)>8) "
        End If

        If CheckNome.Checked Then
            filtriQuery = filtriQuery + " and ( name like '" + TextNome.Text + "' or surname like '" + TextNome.Text + "' )"
        End If

        Return filtriQuery
    End Function

    Protected Function calcolaTipoCampagna() As String
        If RadioMAIL.Checked Then
            Return "EMAIL"
        ElseIf RadioPASS.Checked Then
            Return "PASS"
        ElseIf RadioSMS.Checked Then
            Return "SMS"
        End If

        Return "ALTRO"

    End Function

End Class

