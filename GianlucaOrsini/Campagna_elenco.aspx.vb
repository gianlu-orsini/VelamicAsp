﻿Imports System.Data.SqlClient

Public Class Campagna_elenco
    Inherits System.Web.UI.Page

    Dim username As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("username")

        Dim queryString As String = "SELECT id,name,type,datacreazione,numeroInvii,dataUltimoInvio from CAMPAGNE WHERE father like @ID ;"


        Using mySqlConnection As New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")
            Dim command As New SqlCommand(queryString, mySqlConnection)
            Dim DataSet1 As New DataSet
            command.Parameters.Add("@ID", SqlDbType.Text)
            command.Parameters("@ID").Value = username

            mySqlConnection.Open()

            Dim MonAdapteur As SqlDataAdapter = New SqlDataAdapter(command)
            MonAdapteur.Fill(DataSet1)
            command.Dispose()

            If DataSet1.Tables.Count > 0 Then


                Dim table As New DataTable
                table = DataSet1.Tables(0)
                GridView1.DataSource = table
                GridView1.DataBind()
                lblNumeroContatti.Text = "Numero Campagne Presenti : " + table.Rows.Count.ToString
            End If
        End Using



    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub GridView1_Sorting(sender As Object, e As GridViewSortEventArgs) Handles GridView1.Sorting
        'Dim queryString As String = "SELECT * from customers WHERE father like @ID order by email;"


        'Using mySqlConnection As New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")
        '    Dim command As New SqlCommand(queryString, mySqlConnection)
        '    Dim DataSet1 As New DataSet
        '    command.Parameters.Add("@ID", SqlDbType.Text)
        '    command.Parameters("@ID").Value = username

        '    mySqlConnection.Open()

        '    Dim MonAdapteur As SqlDataAdapter = New SqlDataAdapter(command)
        '    MonAdapteur.Fill(DataSet1)
        '    command.Dispose()

        '    If DataSet1.Tables.Count > 0 Then
        '        lblNumeroContatti.Text = "Numero contatti Presenti : " + DataSet1.Tables.Count.ToString

        '        Dim table As New DataTable
        '        table = DataSet1.Tables(0)
        '        GridView1.DataSource = table
        '        GridView1.DataBind()
        '    End If
        'End Using
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As GridViewPageEventArgs) Handles GridView1.SelectedIndexChanged
        'Dim queryString As String = "SELECT * from customers WHERE father like @ID order by name;"


        'Using mySqlConnection As New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")
        '    Dim command As New SqlCommand(queryString, mySqlConnection)
        '    command.Parameters.Add("@ID", SqlDbType.Text)
        '    command.Parameters("@ID").Value = username

        '    mySqlConnection.Open()
        '    Dim reader As SqlDataReader = command.ExecuteReader()
        '    If reader.HasRows Then
        '        GridView1.DataSource = reader
        '        GridView1.DataBind()
        '    End If
        '    reader.Close()

        'End Using
    End Sub

    'Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
    '    Response.Redirect("database_add.aspx?username=" + username)
    'End Sub

    'Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
    '    Response.Redirect("dashboard.aspx?username=" + username)
    'End Sub

    'Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
    '    Dim queryString As String = "DELETE FROM customers WHERE father like @FATHER"

    '    Using mySqlConnection As New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")
    '        Dim command As New SqlCommand(queryString, mySqlConnection)

    '        mySqlConnection.Open()

    '        command.Parameters.AddWithValue("@FATHER", username)
    '        Try
    '            command.ExecuteNonQuery()

    '            Button2.Visible = True
    '        Catch ex As Exception
    '            Label2.Text = ex.Message
    '        End Try


    '    End Using
    'End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Response.Redirect("dashboard.aspx?username=" + username)
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("creaCampagna.aspx?username=" + username)
    End Sub
End Class