﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Home.aspx.vb" Inherits="GianlucaOrsini.Home" MasterPageFile="~/Site1.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentMenu">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>
    <asp:sitemapdatasource id="SiteMapDataSource1" runat="server" sitemapprovider="" showstartingnode="false" startingnodeoffset="0" />
        <div id="Page">
           <div id="Container">
               <table style="width: 310px;">
                   <tr>
                       <td style="width:190px" class="logoMenu"></td>
                       <td style="width:60px" class="labelBianca">PANNELLO</td>
                       <td style="width:60px" class="labelBianca">LOGIN</td>
                   </tr>
               </table>
            </div>
        </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <script type="text/javascript" src="script/Home.js"></script>
    <link href="cssMenu.css" rel="stylesheet" type="text/css" />
    <div>
            <asp:Label ID="Label1" runat="server" Text="Nickname" ToolTip="Inserire il nickName" Width="120px" CssClass="labelMedia"></asp:Label>
            <asp:TextBox ID="TxtNickName" runat="server" CssClass="txtMedia"></asp:TextBox>
            <br />
            <asp:Label ID="Label2" runat="server" Text="Password" ToolTip="Inserire la PAssword" Width="120px" CssClass="labelMedia" ></asp:Label>
            <asp:TextBox ID="TxtPassword" runat="server" CssClass="txtMedia"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="Login" OnClientClick="return checkLogin();" />
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" Text="Label" Visible="False"></asp:Label>           
    </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    <asp:Button ID="Button2" runat="server" Text="Mappa" />
</asp:Content>
