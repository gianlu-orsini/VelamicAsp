﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="creaCampagna.aspx.vb" Inherits="GianlucaOrsini.creaCampagna" MasterPageFile="~/Site1.Master"%>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <script type="text/javascript" src="script/creaCampagna.js"></script>
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="X-Large" Text="NOME CAMPAGNA :"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="textName" runat="server" Width="231px"></asp:TextBox>
        <br />
        <br />

        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Large" Text="TIPOLOGIA DI CAMPAGNA :"></asp:Label>
        <br />
        <br />
        <asp:RadioButton ID="RadioSMS" runat="server" AutoPostBack="True" Checked="True" Text="SMS" />
        <br />
        <asp:RadioButton ID="RadioMAIL" runat="server" AutoPostBack="True" Text="EMAIL" />
        <br />
        <asp:RadioButton ID="RadioPASS" runat="server" AutoPostBack="True" Text="PASS" />
        <br />
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="X-Large" Text="FILTRI CAMPAGNA :"></asp:Label>
        <br />
        <br />
        <asp:CheckBox ID="CheckSesso" runat="server" AutoPostBack="True" Text="Sesso" />
&nbsp;&nbsp;&nbsp;
        <asp:RadioButton ID="RadioMaschio" runat="server" AutoPostBack="True" Checked="True" Text="Maschio" />
&nbsp;&nbsp;&nbsp;
        <asp:RadioButton ID="RadioFemmina" runat="server" AutoPostBack="True" Text="Femmina" />
        <br />
        <br />
        <asp:CheckBox ID="CheckNome" runat="server" AutoPostBack="True" Text="Nome" />
&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="TextNome" runat="server" ToolTip="Inserire il nome per il filtro"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Applica Filtri" Width="217px" />
        <br />
        <br />
        <br />
        <asp:Label ID="lblNumeroContatti" runat="server" Font-Bold="True" Font-Size="Large"></asp:Label>
        <br />
        <br />
        <br />
        <asp:Button ID="Button3" runat="server" Text="Salva Campagna" OnClientClick="return checkCampi()"/>
&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" Text="Annulla" />
        <br />

    </div>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    <div>  
      <asp:GridView ID="GridView1" runat="server" 
            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" AllowPaging="True" PageSize="20">  
            <Columns>  
                <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True"  ItemStyle-Width="150" SortExpression="name" >  
                <FooterStyle BackColor="#FF0066" />
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="surname" HeaderText="Surname"   ItemStyle-Width="150" SortExpression="surname" >  
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="email" HeaderText="Email"   ItemStyle-Width="150" SortExpression="email" >  
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="cell" HeaderText="Phone"   ItemStyle-Width="150" SortExpression="cell" >  
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="address" HeaderText="Address"   ItemStyle-Width="150" SortExpression="address" >   
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
            </Columns>  
            <PagerSettings PageButtonCount="30" />
        </asp:GridView>    
    </div>

</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentMenu">
    <link href="cssMenu.css" rel="stylesheet" type="text/css" />
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>
    <asp:sitemapdatasource id="SiteMapDataSource1" runat="server" sitemapprovider="" showstartingnode="false" startingnodeoffset="0" />
        <div id="Page">
           <div id="Container">
            <asp:menu id="Menu1" runat="server" datasourceid="SiteMapDataSource1" orientation="Horizontal" width="95%" staticdisplaylevels="1" Height="30px">
               <dynamicmenustyle />
            </asp:menu>
            <div class="Breadcrumbs">
                <asp:sitemappath id="SiteMapPath1" runat="server"></asp:sitemappath>
            </div>
           </div>
        </div>
</asp:Content>