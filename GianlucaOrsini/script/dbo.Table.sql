﻿CREATE TABLE [dbo].[customers]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [companyname] TEXT NULL, 
    [contactname] TEXT NULL, 
    [address] TEXT NULL, 
    [city] TEXT NULL
)
