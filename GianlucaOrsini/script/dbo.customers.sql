﻿CREATE TABLE [dbo].[customers] (
    [Id]      INT  NOT NULL IDENTITY,
    [name]    TEXT NULL,
    [surname] TEXT NULL,
    [email]   TEXT NULL,
    [cell]    TEXT NULL,
    [address] TEXT NULL,
    [father]  TEXT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC )
);

