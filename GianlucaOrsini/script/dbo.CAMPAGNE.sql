﻿CREATE TABLE [dbo].[CAMPAGNE] (
    [Id]              INT      IDENTITY (1, 1) NOT NULL,
    [father]          TEXT     NULL,
    [query]           TEXT     NULL,
    [datacreazione]   DATETIME NULL,
    [dataUltimoInvio] DATETIME NULL,
    [numeroInvii]     INT      NULL,
    [name] TEXT NULL, 
    [type] TEXT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

