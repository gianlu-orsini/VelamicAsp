﻿
function checkAddNominativo() {

    var textbox1 = document.forms[0]['ctl00$ContentPlaceHolder1$txtNome'];
    var textbox2 = document.forms[0]['ctl00$ContentPlaceHolder1$txtCognome'];
    var textbox3 = document.forms[0]['ctl00$ContentPlaceHolder1$txtEmail'];
    var textbox4 = document.forms[0]['ctl00$ContentPlaceHolder1$txtCellulare'];
    var textbox5 = document.forms[0]['ctl00$ContentPlaceHolder1$txtIndirizzo'];

    if (textbox1.value == '') {
        alert('Il campo Nome non puo essere vuoto');
        return false;
    }
    if (textbox2.value == '') {
        alert('Il campo Cognome non puo essere vuoto');
        return false;
    }
    if (textbox3.value == '') {
        alert('Il campo Email non puo essere vuoto');
        return false;
    }
    if (textbox4.value == '') {
        alert('Il campo Cellulare non puo essere vuoto');
        return false;
    }
    if (textbox5.value == '') {
        alert('Il campo Indirizzo non puo essere vuoto');
        return false;
    }

    return true;
}