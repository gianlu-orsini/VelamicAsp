Option Explicit On
Option Strict On

Imports System
Imports System.IO
Imports opencomm.global.Costanti

Public Class Utility

    Private Const nomeModulo As String = "opencomm.global.utility."
    Private Shared _ErrFolder As String = ""
    Private Const _DateNull As String = "01/01/0001"


    Public Shared Function StreamClose(ByRef mySr As StreamReader) As Boolean
        Try
            If Not mySr Is Nothing Then
                mySr.Close()
            End If
        Catch ex As Exception
        Finally
            mySr = Nothing
        End Try
        Return True
    End Function

    Public Shared Function StreamClose(ByRef mySr As StreamWriter) As Boolean
        Try
            If Not mySr Is Nothing Then
                mySr.Close()
            End If
        Catch ex As Exception
        Finally
            mySr = Nothing
        End Try
        Return True
    End Function

    Public Shared Function StreamOpen(ByRef mySr As IO.StreamReader, ByVal sNomeFile As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Boolean
        Const nomeFunction As String = nomeModulo & "bStreamOpen"
        Try
            mySr = New IO.StreamReader(sNomeFile)
        Catch ex As IO.FileLoadException
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Catch ex As IO.FileNotFoundException
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Catch ex As IO.IOException
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Catch ex As Exception
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        End Try

        Return Not isErr
    End Function

    ''' <summary>
    ''' Applica il formato "codice" al campo passato in input
    ''' </summary>
    ''' <param name="Input">campo da formattare</param>
    ''' <param name="NumCar">numero di caratteri da restituire</param>
    ''' <param name="carFill">carattere di riempimento</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaCodeFormat(ByVal Input As Object, ByVal NumCar As Int32, ByVal carFill As Char) As String
        Return String.Format("{0:" & New String(carFill, NumCar) & "}", Input)
    End Function

    ''' <summary>
    ''' applica il formato data con anno a 4 cifre
    ''' </summary>
    ''' <param name="sInput"></param>
    ''' <param name="sFormatoNew"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaDataFormat(ByVal sInput As Object, Optional ByVal sFormatoNew As String = "") As String
        If Convert.ToString(sInput).Trim.Length <= 0 Then
            Return ""
        Else
            If sFormatoNew <> "" Then
                Return String.Format("{0:" & sFormatoNew & "}", Convert.ToDateTime(sInput))
            Else
                Return String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(sInput))
            End If
        End If
    End Function

    ''' <summary>
    ''' applica la data in formato americano
    ''' </summary>
    ''' <param name="sInput"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaDataFormat2(ByVal sInput As Object) As String
        If Convert.ToString(sInput).Trim.Length <= 0 Then
            Return ""
        Else
            Return String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(sInput))
        End If
    End Function

    ''' <summary>
    ''' applica il formtato valuta
    ''' </summary>
    ''' <param name="sInput"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaCurrencyFormat(ByVal sInput As Object) As String
        Try
            Return String.Format("{0:#,##0.00000}", Convert.ToDecimal(sInput))
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    ''' <summary>
    ''' restituisce la stringa per formattare un campo come Currency
    ''' </summary>
    ''' <param name="numDec">numero di decimali che si vuole applicare</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStringCurrencyFormat(Optional ByVal numDec As Integer = 3, Optional ByVal ZeroVuoto As Boolean = False) As String
        If ZeroVuoto Then
            If numDec <= 0 Then
                Return "#,##0;-#,##0; "
            Else
                Return "#,##0." & New String(CType("0", Char), numDec) & ";" & "-#,##0." & New String(CType("0", Char), numDec) & "; "
            End If
        Else
            If numDec <= 0 Then
                Return "#,##0"
            Else
                Return "#,##0." & New String(CType("0", Char), numDec)
            End If
        End If
    End Function


    ''' <summary>
    ''' appliva il formato percentuale
    ''' </summary>
    ''' <param name="sInput"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaPercFormat(ByVal sInput As Object) As String
        Try
            Return String.Format("{0:##0.00}", Convert.ToDecimal(sInput))
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    ''' <summary>
    ''' Restituisce la stringa per la formattazione dei valori percentuali
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStringPercFormat() As String
        Return "##0.00"
    End Function

    ''' <summary>
    ''' applica un foramto decimale con un numero di decimali predefinito
    ''' </summary>
    ''' <param name="sInput"></param>
    ''' <param name="iNumDecimal"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaDecimalFormat(ByVal sInput As Object, Optional ByVal iNumDecimal As Int32 = 2) As String
        Try
            Return String.Format("{0:##0." & New String(CType("0", Char), iNumDecimal) & "}", Convert.ToDecimal(sInput))
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    ''' <summary>
    ''' applica un foramto decimale con un numero di decimali predefinito in visualizzazione
    ''' </summary>
    ''' <param name="sInput"></param>
    ''' <param name="iNumDecimal"></param>
    ''' <param name="bZeroIsVuoto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaDecimalFormatVis(ByVal sInput As Object, Optional ByVal iNumDecimal As Int32 = 2, Optional ByVal bZeroIsVuoto As Boolean = False) As String
        Try
            If bZeroIsVuoto AndAlso Convert.ToDecimal(sInput) = 0 Then
                Return ""
            Else
                Return String.Format("{0:#,##0." & New String(CType("0", Char), iNumDecimal) & "}", Convert.ToDecimal(sInput))
            End If
        Catch ex As Exception
            If bZeroIsVuoto Then
                Return ""
            Else
                Return "0"
            End If
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="numDec"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStringDecimalFormat(Optional ByVal numDec As Int32 = 3, Optional ByVal ZeroVuoto As Boolean = False) As String
        If ZeroVuoto Then
            If numDec <= 0 Then
                Return "#,##0;-#,##0; "
            Else
                Return "#,##0." & New String(CType("0", Char), numDec) & ";" & "-#,##0." & New String(CType("0", Char), numDec) & "; "
            End If
        Else
            If numDec <= 0 Then
                Return "#,##0"
            Else
                Return "#,##0." & New String(CType("0", Char), numDec)
            End If
        End If
    End Function

    ''' <summary>
    ''' applica un foramto decimale con un numero di decimali predefinito
    ''' </summary>
    ''' <param name="sInput"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaDecimalFormatVariabile(ByVal sInput As Object) As String
        Try
            Return String.Format("{0:##0.#}", Convert.ToDecimal(sInput))
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStringDecimalFormatVariabile() As String
        Return "#,##0.#"
    End Function

    ''' <summary>
    ''' applica un foramto decimale con un numero di decimali predefinito in visulizzazione
    ''' </summary>
    ''' <param name="sInput"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaDecimalFormatVariabileVis(ByVal sInput As Object) As String
        Try
            Return String.Format("{0:#,##0.#}", Convert.ToDecimal(sInput))
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    ''' <summary>
    ''' applica il formato hh:mm
    ''' </summary>
    ''' <param name="sInput"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplicaOraFormat(ByVal sInput As Object) As String
        If Convert.ToString(sInput).Trim.Length <= 0 Then
            Return ""
        Else
            Return String.Format("{0:hh:mm}", Convert.ToDateTime(sInput))
        End If
    End Function

    Public Shared Function getErrore(ByVal nomeFunction As String, Optional ByVal errMsg As String = "") As String
        Dim sErroreEsteso As String = ""
        getErrore = nomeFunction & Convert.ToString(IIf(errMsg = "", "", vbCrLf & errMsg))
        sErroreEsteso = vbCrLf & getErrore
        sErroreEsteso &= vbCrLf & "***"
        WriteErrOnFile(sErroreEsteso)
    End Function

    Public Shared Function getErrore(ByVal nomeFunction As String, ByVal ex As Exception, Optional ByVal errMsg As String = "") As String
        Dim sErroreEsteso As String = ""
        If Not ex Is Nothing Then
            getErrore = nomeFunction & " [" & ex.Source & "]: " & ex.Message & Convert.ToString(IIf(errMsg = "", "", vbCrLf & errMsg))
            sErroreEsteso = vbCrLf & "Errore applicazione"
            sErroreEsteso &= vbCrLf & String.Format("{0:yyyy-MM-dd HH.mm.ss}", DateTime.Now)
            sErroreEsteso &= vbCrLf & getErrore.Trim
            sErroreEsteso &= vbCrLf & "Stack Trace: " & ex.StackTrace.Trim
        Else
            getErrore = nomeFunction & Convert.ToString(IIf(errMsg = "", "", vbCrLf & errMsg))
            sErroreEsteso = vbCrLf & getErrore
        End If
        sErroreEsteso &= vbCrLf & "***"
        WriteErrOnFile(sErroreEsteso)
    End Function

    Public Shared Function getSqlErrore(ByVal nomeFunction As String, ByVal ex As SqlClient.SqlException, ByVal mySql As String, Optional ByVal errMsg As String = "") As String
        Dim sErroreEsteso As String = ""
        If Not ex Is Nothing Then
            getSqlErrore = nomeFunction & " [" & ex.Source & "]: " & ex.Message & vbCrLf & Convert.ToString(IIf(errMsg = "", "", vbCrLf & errMsg))
            sErroreEsteso = vbCrLf & "Errore SqlClient"
            sErroreEsteso &= vbCrLf & String.Format("{0:yyyy-MM-dd HH.mm.ss}", DateTime.Now)
            sErroreEsteso &= vbCrLf & getSqlErrore.Trim
            sErroreEsteso &= vbCrLf & "Comando Sql: " & mySql.Trim
            sErroreEsteso &= vbCrLf & "Stack Trace: " & ex.StackTrace.Trim
            sErroreEsteso &= vbCrLf & "Classe: " & ex.Class.ToString.Trim
            sErroreEsteso &= vbCrLf & "Numero Errori: " & ex.Errors.Count.ToString.Trim
            sErroreEsteso &= vbCrLf & "Linea Sql Erorre: " & ex.LineNumber.ToString.Trim
            sErroreEsteso &= vbCrLf & "Numero Errore: " & ex.Number.ToString.Trim
            sErroreEsteso &= vbCrLf & "Procedure: " & ex.Procedure.Trim
            sErroreEsteso &= vbCrLf & "Server: " & ex.Server.Trim
            sErroreEsteso &= vbCrLf & "Source: " & ex.Source.Trim
            sErroreEsteso &= vbCrLf & "Stack: Trace" & ex.StackTrace.Trim
            sErroreEsteso &= vbCrLf & "Sql State: " & ex.State.ToString.Trim
        Else
            getSqlErrore = nomeFunction & Convert.ToString(IIf(errMsg = "", "", vbCrLf & errMsg))
            sErroreEsteso = vbCrLf & getSqlErrore
        End If
        sErroreEsteso &= vbCrLf & "***"
        WriteErrOnFile(sErroreEsteso)
    End Function

    Public Shared Function WriteErrOnFile(ByVal sErrore As String) As Boolean
        Dim myStream As IO.StreamWriter = Nothing
        Try
            myStream = New StreamWriter(_ErrFolder & "\" & String.Format("{0:yyyy-MM-dd}", DateTime.Now) & ".txt", True)
            If Not myStream Is Nothing Then
                myStream.WriteLine(sErrore)
            End If
        Catch ex As Exception
        Finally
            StreamClose(myStream)
        End Try
    End Function

    Public Shared Function SetErrFolder(ByVal sErrFolder As String) As Boolean
        Try
            _ErrFolder = sErrFolder
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Shared Function StrSQLTipoFld(ByVal sValue As String, ByVal iTipoFld As eGblSqlTipoFld, ByVal isItaliano As Boolean, Optional ByVal iSQLOperatore As eGblSQLOperatore = eGblSQLOperatore.eGblSQLOperNone) As String

        Dim sSQL As String = ""
        Dim sCarData As String = ""
        Dim sSepTime As String = ""
        Const sSepData As String = "-"

        Select Case iTipoFld
            Case eGblSqlTipoFld.eGblSqlTipoFldNum
                If IsNumeric(sValue) Then
                    sSQL = sValue
                Else
                    sSQL = "0"
                End If
            Case eGblSqlTipoFld.eGblSqlTipoFldDec
                Try
                    Convert.ToDecimal(sValue)
                    sSQL = sValue.ToString.Replace(".", "").Replace(",", ".")
                Catch ex As Exception
                    sSQL = sValue
                End Try
            Case eGblSqlTipoFld.eGblSqlTipoFldTesto, eGblSqlTipoFld.eGblSqlTipoFldMemo
                sSQL = StrSQLApici(sValue, iSQLOperatore)
            Case eGblSqlTipoFld.eGblSqlTipoFldBoolean
                If IsNumeric(sValue) Then
                    sSQL = CStr(IIf(Val(sValue) = 0, "0", "1"))
                Else
                    sSQL = StrSQLApici(sValue, iSQLOperatore)
                End If
            Case eGblSqlTipoFld.sGblSqlTipoFldOption
                If IsNumeric(sValue) Then
                    sSQL = sValue
                Else
                    sSQL = StrSQLApici(sValue, iSQLOperatore)
                End If
            Case eGblSqlTipoFld.eGblSqlTipoFldData, eGblSqlTipoFld.eGblSqlTipoFldOra, eGblSqlTipoFld.eGblSqlTipoFldDataOra
                If IsDate(sValue) AndAlso Not sValue.TrimEnd.ToUpper.Equals(_DateNull.TrimEnd.ToUpper) Then
                    sCarData = "'"
                    If iTipoFld <> eGblSqlTipoFld.eGblSqlTipoFldData Then sSepTime = ":"
                    If iTipoFld <> eGblSqlTipoFld.eGblSqlTipoFldOra Then
                        If isItaliano Then
                            sSQL = Format(Year(CDate(sValue)), "0000") & sSepData & Format(Microsoft.VisualBasic.Day(CDate(sValue)), "00") & sSepData & Format(Month(CDate(sValue)), "00")
                        Else
                            sSQL = Format(Month(CDate(sValue)), "00") & sSepData & Format(Microsoft.VisualBasic.Day(CDate(sValue)), "00") & sSepData & Format(Year(CDate(sValue)), "0000")
                        End If
                        If iTipoFld = eGblSqlTipoFld.eGblSqlTipoFldDataOra Then sSQL &= " "
                    End If
                    If iTipoFld <> eGblSqlTipoFld.eGblSqlTipoFldData Then
                        sSQL &= Format(Hour(CDate(sValue)), "00") & sSepTime & Format(Minute(CDate(sValue)), "00") & sSepTime & Format(Second(CDate(sValue)), "00")
                    End If
                    sSQL = sCarData & sSQL & sCarData
                Else
                    sSQL = "NULL"
                End If
            Case eGblSqlTipoFld.eGblSqlTipoFldExtra
                sSQL = sValue
        End Select
        Return sSQL

    End Function

    Private Shared Function StrSQLApici(ByVal sValue As String, Optional ByVal iSQLOperatore As eGblSQLOperatore = eGblSQLOperatore.eGblSQLOperNone) As String

        Const sCarLike As String = "%"
        sValue = Replace(sValue, "'", "''")
        ' Rimpiazzo il simbolo �$� con apice singolo (ST 9/3/2005)
        sValue = Replace(sValue, "�$�", "'")

        If iSQLOperatore = eGblSQLOperatore.eGblSQLOperLike And Left(sValue, 1) <> sCarLike Then sValue = sCarLike & sValue
        If (iSQLOperatore = eGblSQLOperatore.eGblSQLOperBegin Or iSQLOperatore = eGblSQLOperatore.eGblSQLOperLike) And _
         Right(sValue, 1) <> sCarLike Then sValue &= sCarLike

        Return Chr(39) & sValue & Chr(39)

    End Function

    Public Shared Function Fld2Short(ByVal myVal As Object) As Short
        Dim myApp As Short = 0
        Try
            myApp = CType(myVal, Short)
        Catch ex As Exception
            myApp = 0
        End Try
        Return myApp
    End Function

    Public Shared Function Fld2Int(ByVal myVal As Object) As Integer
        Dim myApp As Integer = 0
        Try
            myApp = CType(myVal, Integer)
        Catch ex As Exception
            myApp = 0
        End Try
        Return myApp
    End Function

    Public Shared Function Fld2Dec(ByVal myVal As Object) As Decimal
        Dim myApp As Decimal = 0
        Try
            myApp = CType(myVal, Decimal)
        Catch ex As Exception
            myApp = 0
        End Try
        Return myApp
    End Function

    Public Shared Function Fld2Dbl(ByVal myVal As Object) As Double
        Dim myApp As Double = 0
        Try
            myApp = CType(myVal, Double)
        Catch ex As Exception
            myApp = 0
        End Try
        Return myApp
    End Function

    Public Shared Function Fld2Sng(ByVal myVal As Object) As Single
        Dim myApp As Single = 0
        Try
            myApp = CType(myVal, Single)
        Catch ex As Exception
            myApp = 0
        End Try
        Return myApp
    End Function

    ''' <summary>
    ''' prova a convertire un valore in stringa, in caso di errore viene restituito ""
    ''' </summary>
    ''' <param name="myVal">valore da convertire</param>
    ''' <param name="noLTrim">se false restituisce il left trim del valore di input</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Fld2Str(ByVal myVal As Object, ByVal noLTrim As Boolean) As String
        Dim myApp As String = ""
        Try
            myApp = CType(myVal, String)
        Catch ex As Exception
            myApp = ""
        End Try
        Try
            If Not noLTrim Then
                Return myApp.Trim
            Else
                Return myApp.TrimEnd
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function Fld2Str(ByVal myVal As Object) As String
        Dim myApp As String = ""
        Try
            Return Fld2Str(myVal, False)
        Catch ex As Exception
            Return ""
        End Try
    End Function

    ''' <summary>
    ''' prova a convertire un valore in un boolean, in caso di errore restituisce false
    ''' </summary>
    ''' <param name="myVal">valore da convertire</param>
    ''' <returns>un campo di tipo boolean</returns>
    ''' <remarks>in caso di errore viene restituito false</remarks>
    Public Shared Function Fld2Bool(ByVal myVal As Object) As Boolean
        Dim myApp As Int32 = 0
        Try
            myApp = CType(myVal, Integer)
        Catch ex As Exception
            myApp = 0
        End Try
        Return (myApp <> 0)
    End Function

    ''' <summary>
    ''' prova a convertire un valore in una data, inc aso di errore restituisce Date.MinValue
    ''' </summary>
    ''' <param name="myVal">valore da convertire</param>
    ''' <returns>un campo di tipo data</returns>
    ''' <remarks>in caso di errore viene restituito Date.MinValue</remarks>
    Public Shared Function Fld2Date(ByVal myVal As Object) As Date
        Dim myApp As Date = Date.MinValue
        Try
            myApp = Convert.ToDateTime(myVal).Date
        Catch ex As Exception
            myApp = Date.MinValue
        End Try
        Return myApp
    End Function

    ''' <summary>
    ''' prova a convertire un valore in datetime, in caso di errore restituisce DateTime.MinValue
    ''' </summary>
    ''' <param name="myVal">valore da convertire</param>
    ''' <returns>un campo di tipo DateTime</returns>
    ''' <remarks>in caso di errore viene restituito DateTime.MinValue</remarks>
    Public Shared Function Fld2DateTime(ByVal myVal As Object) As Date
        Dim myApp As DateTime = DateTime.MinValue
        Try
            myApp = Convert.ToDateTime(myVal)
        Catch ex As Exception
            myApp = DateTime.MinValue
        End Try
        Return myApp
    End Function

    ''' <summary>
    ''' restituisce una stringa Sql formattata per il tipo di campo del DB
    ''' </summary>
    ''' <param name="value">valore da convertire, deve essere un valore convertibile in stringa</param>
    ''' <param name="isItaliano">opzionale, se vero indica che il db in uso � in italiano, default = vero</param>
    ''' <param name="tipoFld">specifica il tipo di formattazione che si vuole ottenere</param>
    ''' <param name="sqlOperatore">specifica l'operatore Sql da gestire</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function sqlString(ByVal value As Object, Optional ByVal isItaliano As Boolean = False, Optional ByVal tipoFld As eGblSqlTipoFld = eGblSqlTipoFld.eGblSqlTipoFldTesto, Optional ByVal sqlOperatore As eGblSQLOperatore = eGblSQLOperatore.eGblSQLOperNone) As String
        Dim myValue As String = ""
        Try
            myValue = Convert.ToString(value)
        Catch ex As Exception
            myValue = ""
        End Try
        Return StrSQLTipoFld(myValue, tipoFld, isItaliano, sqlOperatore)
    End Function


    Public Shared Function CloseCmd(ByRef myCmd As SqlClient.SqlCommand, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Boolean
        Const nomeFunction As String = nomeModulo & "bCloseCmd"
        Try

            If Not myCmd Is Nothing Then
                Try
                    myCmd.Dispose()
                Catch ex As Exception
                Finally
                    myCmd = Nothing
                End Try
            End If

        Catch ex As SqlClient.SqlException
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Catch ex As Exception
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        End Try

        Return Not isErr
    End Function

    Public Shared Function CloseConn(ByRef myCnnSql As SqlClient.SqlConnection, Optional ByRef bNoSetNothing As Boolean = False, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Boolean
        Const nomeFunction As String = nomeModulo & "CloseConn"
        Try

            If Not myCnnSql Is Nothing Then
                Try
                    myCnnSql.Close()
                    If Not bNoSetNothing Then
                        Try
                            myCnnSql.Dispose()
                        Catch ex As Exception
                        Finally
                            myCnnSql = Nothing
                        End Try
                    End If
                Catch ex As Exception
                End Try
            End If

        Catch ex As SqlClient.SqlException
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Catch ex As Exception
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        End Try

        Return Not isErr
    End Function

    Public Shared Function CloseConn(ByRef myCnnSql As OleDb.OleDbConnection, Optional ByRef bNoSetNothing As Boolean = False, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Boolean
        Const nomeFunction As String = nomeModulo & "CloseConn"
        Try

            If Not myCnnSql Is Nothing Then
                Try
                    myCnnSql.Close()
                    If Not bNoSetNothing Then
                        Try
                            myCnnSql.Dispose()
                        Catch ex As Exception
                        Finally
                            myCnnSql = Nothing
                        End Try
                    End If
                Catch ex As Exception
                End Try
            End If

        Catch ex As SqlClient.SqlException
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Catch ex As Exception
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        End Try

        Return Not isErr
    End Function

    Public Shared Function CloseDs(ByRef myDs As DataSet, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Boolean
        Const nomeFunction As String = nomeModulo & "CloseDs"
        Try
            If Not myDs Is Nothing Then
                myDs.Clear()
                myDs.Dispose()
            End If
        Catch ex As SqlClient.SqlException
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Catch ex As Exception
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Finally
            myDs = Nothing
        End Try
        Return Not isErr
    End Function

    Public Shared Function CloseDt(ByRef myDt As DataTable, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Boolean
        Const nomeFunction As String = nomeModulo & "CloseDt"
        Try
            If Not myDt Is Nothing Then
                myDt.Clear()
                myDt.Dispose()
            End If
        Catch ex As SqlClient.SqlException
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Catch ex As Exception
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Finally
            myDt = Nothing
        End Try
        Return Not isErr
    End Function

    Public Shared Function CloseDv(ByRef myDv As DataView, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Boolean
        Const nomeFunction As String = nomeModulo & "CloseDv"
        Try
            If Not myDv Is Nothing Then
                myDv.Dispose()
            End If
        Catch ex As SqlClient.SqlException
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Catch ex As Exception
            errMsg = getErrore(nomeFunction, ex, errMsg)
            isErr = True
        Finally
            myDv = Nothing
        End Try
        Return Not isErr
    End Function



    ''' <summary>
    ''' metodo che data la lunghezza di una stringa in caratteri ritorna il fattore di conversione in pixel
    ''' </summary>
    ''' <param name="iLenght">lunghezza della stringa da convertire </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConvToPxl(ByVal iLenght As Integer, ByVal sParam As String) As Integer

        Try
            Dim valori() As String = Nothing

            For Each fascia As String In sParam.Split("|".ToCharArray)
                valori = fascia.Split(";".ToCharArray)

                If Not IsNothing(valori) AndAlso valori.GetLength(0) = 2 Then
                    If iLenght <= Val(valori(0)) Then Return CInt(valori(1))
                End If
            Next

            Return 0

        Catch ex As Exception
        End Try
    End Function


    'cancella un numero prefissato di file partendo dal pi� vecchio
    Public Shared Function DeleteNFile(ByVal path As String, ByVal nMaxFile As Int32) As Boolean
        Dim sFile As String = ""
        Dim j As Int32 = 0
        If System.IO.Directory.GetFiles(path).GetUpperBound(0) + 1 > nMaxFile Then
            'procedo alla cancellazione
            For j = 0 To System.IO.Directory.GetFiles(path).GetUpperBound(0) - nMaxFile
                DeleteSingleFile(System.IO.Directory.GetFiles(path)(j))
            Next
        End If

    End Function

    'metodo che cacella da una data directory il file o i file specificati
    Public Shared Function DeleteFile(ByVal path As String, Optional ByVal fileName As String = "", Optional ByVal ext As String = "", Optional ByVal minOld As Int32 = Int32.MinValue) As Boolean
        Dim bOk As Boolean = False
        bOk = True
        If ((fileName.Trim.Length <= 0) AndAlso (ext.Trim.Length <= 0)) Then Return False
        If fileName.Trim.Length > 0 Then
            If minOld >= 0 Then
                If DateTime.Now.Subtract(System.IO.File.GetCreationTime(fileName)).TotalMinutes > minOld Then
                    bOk = DeleteSingleFile(fileName)
                End If
            Else
                bOk = DeleteSingleFile(fileName)
            End If
        Else
            Dim sFile As String = ""
            For Each sFile In System.IO.Directory.GetFiles(path, ext)
                If minOld >= 0 Then
                    If DateTime.Now.Subtract(System.IO.File.GetCreationTime(sFile)).TotalMinutes > minOld Then
                        bOk = DeleteSingleFile(sFile)
                    End If
                Else
                    bOk = DeleteSingleFile(sFile)
                End If
            Next
            Return bOk
        End If

    End Function

    'metodo che effettua la cancellazione di un singolo file
    Public Shared Function DeleteSingleFile(ByVal nomefile As String) As Boolean
        Try
            If System.IO.File.Exists(nomefile) Then
                System.IO.File.Delete(nomefile)
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    'restituisce vero se il file che erco esiste
    Public Shared Function FileExist(ByVal nomeFile As String) As Boolean
        Try
            Return System.IO.File.Exists(nomeFile)
        Catch ex As Exception
            Return False
        End Try
    End Function

    'eseguo la copia di un file
    Public Shared Function FileCopy(ByVal source As String, ByVal dest As String, Optional ByVal overWrite As Boolean = False) As Boolean
        Try
            System.IO.File.Copy(source, dest, overWrite)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function FolderExist(ByVal folderName As String) As Boolean
        Try
            Return Directory.Exists(folderName)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function CreateFolder(ByVal folderName As String) As Boolean
        Try
            If Not FolderExist(folderName) Then Directory.CreateDirectory(folderName)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function


End Class
