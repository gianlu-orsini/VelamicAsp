﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Le informazioni generali relative a un assembly sono controllate dal seguente 
' insieme di attributi. Per modificare le informazioni associate a un assembly
' occorre quindi modificare i valori di questi attributi.

' Rivedere i valori degli attributi dell'assembly

<Assembly: AssemblyTitle("OC_Global")> 
<Assembly: AssemblyDescription("Funzioni Globali")> 
<Assembly: AssemblyCompany("opencomm")> 
<Assembly: AssemblyProduct("opencomm shared")> 
<Assembly: AssemblyCopyright("Copyright © Opencomm 2008")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'Se il progetto viene esposto a COM, il GUID che segue verrà utilizzato per creare l'ID della libreria dei tipi
<Assembly: Guid("80ca5884-d738-4600-a22d-1bd03c9ea805")> 

' Le informazioni sulla versione di un assembly sono costituite dai seguenti quattro valori:
'
'      Numero di versione principale
'      Numero di versione secondario 
'      Numero build
'      Revisione
'
' È possibile specificare tutti i valori oppure impostare valori predefiniti per i numeri relativi alla revisione e alla build 
' utilizzando l'asterisco (*) come descritto di seguito:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
