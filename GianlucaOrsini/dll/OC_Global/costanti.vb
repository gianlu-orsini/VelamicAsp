Option Explicit On
Option Strict On


Public Class Costanti

#Region " enum "

    ''' <summary>
    ''' Tipi di campi gestiti
    ''' </summary>
    ''' <remarks></remarks>
    Enum eGblSqlTipoFld As Short
        eGblSqlTipoFldNone = -1
        eGblSqlTipoFldTesto = 0
        eGblSqlTipoFldNum
        eGblSqlTipoFldDec
        eGblSqlTipoFldData
        eGblSqlTipoFldOra
        eGblSqlTipoFldDataOra = 5
        eGblSqlTipoFldBoolean
        sGblSqlTipoFldOption
        eGblSqlTipoFldMemo
        eGblSqlTipoFldExtra
    End Enum

    ''' <summary>
    ''' Tipi di operatore utilizzabili nelle query
    ''' </summary>
    ''' <remarks></remarks>
    Enum eGblSQLOperatore As Short
        eGblSQLOperNone = 0 '(Nessuno: usato per gli Option se hanno stringhe SQL associate per i valori)
        eGblSQLOperLike = 1
        eGblSQLOperBegin = 2
        eGblSQLOperEqual = 3
        eGblSQLOperNEqual = 4
        eGblSQLOperFrom = 5 '">="
        eGblSQLOperNFrom = 6 '"<"
        eGblSQLOperTo = 7 '"<="
        eGblSQLOperNTo = 8 '">"
        eGblSQLOperIn = 9
        eGblSQLOperNIn = 10
        eGblSQLOperBetween = 11
        eGblSQLOperNBetween = 12
    End Enum


    ''' <summary>
    ''' Enum con i numeri dei file delle query
    ''' </summary>
    ''' <remarks></remarks>
    Enum eQueryFile As Integer
        qfDefault = 0
        qfGrid
        qfTabDecodifica
        qfDettPagine
        qfPricing
    End Enum

    Enum eImportXlsFile As Integer
        ixfDefault = 0
    End Enum

    Enum eExportXlsFile As Integer
        ixfDefault = 0
    End Enum

    ''' <summary>
    ''' Indica le opzioni per il oampo. I valori che assume sono gestiti a bit
    ''' </summary>
    ''' <remarks></remarks>
    <Flags()> Public Enum eOpzFld As Integer
        ofStandard = 0
        ofSoloNumeri = 1
        ofBlankValido = 2
        ofConfigurabile = 4
        ofExtra = 8
        ofNoNegativi = 16
        ofNoSepMigliaia = 32
        ofGestBit = 64
    End Enum

    ''' <summary>
    ''' Indica il tipo gestito per il campo
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum eTipoFld As Integer
        tfNone = -1
        tfAlfanumerico = 0
        tfInteger = 1
        tfDecimal = 2
        tfBoolean = 3
        tfData = 4
        tfOra = 5
        tfDataOra = 6
    End Enum

    ''' <summary>
    ''' Indica la modalit� di dipendenza da altre Tabelle
    ''' </summary>
    ''' <remarks></remarks>
    <Flags()> Public Enum eFlgTipoFld As Integer
        ftfNonUtilizzato = 0
        ftfCampoAttributo = 1
        ftfCampoChiaveUnivoca = 2
    End Enum

    ''' <summary>
    ''' Indica se il Campo � Tabellato
    ''' </summary>
    ''' <remarks></remarks>
    <Flags()> Public Enum eTipoCode
        tcNoCode = 0
        tcMostraCodice = 1
        tcMostraDescrizione = 2
        tcAppartieneCatene = 4
        tcStoricizzato = 8
    End Enum

    ''' <summary>
    ''' Flag Uso Tabella
    ''' </summary>
    ''' <remarks></remarks>
    <Flags()> Public Enum eFlgTable As Integer
        ftTabellaStandard = 0
        ftTabellaNonUsata = 1
        ftTabellaInLingua = 2
    End Enum

    ''' <summary>
    ''' Indica la Tipologia della Tabella.
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum eTipoTable As Integer
        ttNonTipizzata = 0
        ttDecodificaStandard
        ttDecodificaPersonalizzata
        ttConfigurabile
        ttInesistente
    End Enum

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum eTlbTipoButton As Integer
        tbButton = 0
        tbButtonGroup = 1
        tbCustom = 2
        tbFocusable = 3
        tbLabel = 4
        tbObject = 5
        tbSeparator = 6
        tbTextBox = 7
    End Enum
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum eTipoCtrl As Integer
        ''' <summary>
        ''' Controllo di tipo testo
        ''' </summary>
        ''' <remarks></remarks>
        tcTextEdit = 0
        ''' <summary>
        ''' Controllo di tipo numerico
        ''' </summary>
        ''' <remarks></remarks>
        tcNumEdit
        ''' <summary>
        ''' Controllo di tipo data
        ''' </summary>
        ''' <remarks></remarks>
        tcDCEdit
        ''' <summary>
        ''' controllo di tipo option
        ''' </summary>
        ''' <remarks></remarks>
        tcOBEdit
        ''' <summary>
        ''' controllo di tipo combo
        ''' </summary>
        ''' <remarks></remarks>
        tcCBEdit
        ''' <summary>
        ''' controllo di tipo check box singolo
        ''' </summary>
        ''' <remarks></remarks>
        tcSCehckBoxEdit
        ''' <summary>
        ''' controllo di tipo check box multiplo
        ''' </summary>
        ''' <remarks></remarks>
        tcMCehckBoxEdit
    End Enum

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum eTipoOpzione As Integer
        toNonConf = 0
        toConfNse
    End Enum

    Public Enum eTipoExportReport As Integer
        eTERPdf = 1
        eTERXls
        eTERDoc
    End Enum

    Public Enum eStampaRecord As Integer
        eSRTutti = 1
        eSRSelezione
    End Enum

    ''' <summary>
    ''' Rappresenta il tipo di gestione che � possibile avere sui campi delle pagine
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum eTipoCampoCfg As Integer
        ''' <summary>
        ''' campo visibile per tutti ma in sola lettura
        ''' </summary>
        ''' <remarks></remarks>
        eSolaLettura = 0

        ''' <summary>
        ''' campo modificabile da tutti (quando l'applicazione � in edit)
        ''' </summary>
        ''' <remarks></remarks>
        eModificabile = 1

        ''' <summary>
        ''' campo modificabile solo in sede (quando l'applicazione � in edit)
        ''' </summary>
        ''' <remarks></remarks>
        eModificabileSoloSede = 2

        ''' <summary>
        ''' campo nascosto e quindi non modificabile
        ''' </summary>
        ''' <remarks></remarks>
        eNascosto = 3
    End Enum

#End Region

#Region " costanti "

    Public Shared SQLOperatoreDescr_It As String() = New String() {"default", "contiene", "inizia per", "uguale", _
        "diverso", "da", "<", "fino a", ">", _
        "in", "not in", "fra", "esterno a"}

    Public Shared SQLOperatoreDescr_En As String() = New String() {"default", "like", "begin", "equal", _
        "not equal", "from", "not from", "to", "not to", _
        "in", "not in", "between", "not between"}

    Public Shared MaxGblSQLOperatore As Short = 12

    Public Shared ReadOnly Property SQLOperatoreDescr(ByVal codLinguaIso As String) As String()
        Get
            Select Case codLinguaIso
                Case "IT"
                    Return SQLOperatoreDescr_It
                Case "EN"
                    Return SQLOperatoreDescr_En
            End Select
            Return SQLOperatoreDescr_It
        End Get
    End Property


#End Region

End Class

''' <summary>
''' Azioni Standard Gestite dalla toolbar
''' </summary>
''' <remarks></remarks>
Public Class ToolbarKey

    ''' <summary>
    ''' Esegue l'azione "Nuovo Record"
    ''' </summary>
    ''' <remarks></remarks>
    Public Const [New] As String = "NEW"

    ''' <summary>
    ''' Esegue l'azione "Modifica Record"
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Edit As String = "EDIT"

    ''' <summary>
    ''' Apre la form di ricerca
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Src As String = "SRC"

    ''' <summary>
    ''' Visualizza il dettaglio del record selezionato
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Dett As String = "DETT"

    ''' <summary>
    ''' Elimina il record selezionato
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Del As String = "DEL"

    ''' <summary>
    ''' Autorizzazione record
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Aut As String = "AUT"

    ''' <summary>
    ''' Copia il dettaglio del record selezionato
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Cpy As String = "CPY"

    ''' <summary>
    ''' Import dei dati
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Imp As String = "IMP"

    ''' <summary>
    ''' Conferma
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Conf As String = "CONF"

    ''' <summary>
    ''' Stampa
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Prn As String = "PRN"

    ''' <summary>
    ''' Salvataggio
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Save As String = "SAVE"

    ''' <summary>
    ''' Uscita
    ''' </summary>
    ''' <remarks></remarks>
    Public Const [Exit] As String = "EXIT"

    ''' <summary>
    ''' Avanti - Prossima Scheda
    ''' </summary>
    ''' <remarks></remarks>
    Public Const [Next] As String = "NEXT"

    ''' <summary>
    ''' Indietro - Scheda Precedente
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Previous As String = "PREV"

    ''' <summary>
    ''' Note
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Note As String = "NOTE"

    ''' <summary>
    ''' esegue l'azione "Torna all'elenco"
    ''' </summary>
    ''' <remarks></remarks>
    Public Const Elenco As String = "ELE"

End Class