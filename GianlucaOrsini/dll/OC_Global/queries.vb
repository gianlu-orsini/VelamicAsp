Imports Microsoft.VisualBasic
Imports System
Imports System.Xml
Imports System.Diagnostics
Imports opencomm.global
Imports opencomm.global.Utility
Imports opencomm.global.Costanti
Imports System.Globalization
Imports System.Resources


Public Class Queries
    Private Const nomeModulo As String = "opencomm.global.utility."
    Private Const _replacestring As String = "/queries/query[@id='{0}']"
    Public Shared Event queryGivMeFileName(ByVal numFile As eQueryFile, ByRef nomeFile As String)

    ''' <summary>
    ''' metodo che restituisce la stringa sql corrispondente alla chiave di di ricerca impostata
    ''' </summary>
    ''' <param name="key">Chiave della query da ricercare</param>
    ''' <param name="numFile">Numero del file delle query da utilizzare</param>
    ''' <param name="isErr">Restitisce vero in caso di errore</param>
    ''' <param name="params">Parametri di sostituzione della stringa Sql</param>
    ''' <returns>Stringa Sql Richiesta</returns>
    ''' <remarks></remarks>
    Public Shared Function LoadQuery(ByVal key As String, ByVal numFile As eQueryFile, ByRef isErr As Boolean, ByRef errMsg As String, ByVal ParamArray params() As String) As String
        Const nomeFunction As String = nomeModulo & "LoadQuery"
        Try
            Dim doc As XmlDocument = GetDocument(numFile)
            If Not doc Is Nothing Then
                Dim node As XmlNode = doc.SelectSingleNode(String.Format(_replacestring, key))
                If IsNothing(node) Then
                    isErr = True
                    errMsg = String.Format(oc_global.message.QueryNonTrovata, key)
                    Return ""
                End If
                Dim sql As String = node.InnerText.Trim
                sql = String.Format(sql, params)
                Return sql
            Else
                errMsg = String.Format(oc_global.message.QueryNonTrovata, numFile)
                isErr = True
                Return ""
            End If
            doc = Nothing

        Catch e As FormatException
            isErr = True
            errMsg = String.Format(oc_global.message.QueryParamErr, key)
            Return ""
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, errMsg)
            Return ""
        End Try
    End Function

    ''' <summary>
    ''' Recupera e apre il file delle query
    ''' </summary>
    ''' <param name="numFile">Numero del file da aprire</param>
    ''' <returns>Documento XML con le query</returns>
    ''' <remarks></remarks>
    Private Shared Function GetDocument(ByVal numFile As eQueryFile) As XmlDocument
        Dim doc As New XmlDocument()
        Dim fileName As String = ""
        Try
            RaiseEvent queryGivMeFileName(numFile, fileName)
            If fileName <> "" Then
                doc.Load(fileName)
            End If
            Return doc
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class