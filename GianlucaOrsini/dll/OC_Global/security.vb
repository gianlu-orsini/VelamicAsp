Option Strict On
Option Explicit On

Imports opencomm
Imports opencomm.global
Imports opencomm.global.Utility
Imports System.IO
Imports System.Text

Imports System.Security.Cryptography


''' <summary>
''' Classe per il calcolo della chiave di HASH usando l'algoritmo MD5
''' </summary>
''' <remarks></remarks>
Public Class MD5

    Private Const nomeModulo As String = "opencomm.global.MD5."
    Private _hexEncoding As New HexEncoding

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myStream"></param>
    ''' <param name="myHash"></param>
    ''' <param name="myHashEx"></param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComputeHash(ByRef myStream As IO.Stream, ByRef myHash As String, ByRef myHashEx As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "ComputeHash"
        Dim myRes As Byte() = Nothing
        Dim myMd5 As New MD5CryptoServiceProvider
        Try
            myRes = myMd5.ComputeHash(myStream)
            myHash = Convert.ToBase64String(myRes)
            myHashEx = _hexEncoding.ToString(myRes)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        Finally
            myMd5 = Nothing
        End Try
        Return myRes
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myBuffer"></param>
    ''' <param name="myOffSet"></param>
    ''' <param name="myCount"></param>
    ''' <param name="myHash"></param>
    ''' <param name="myHashEx"></param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComputeHash(ByRef myBuffer() As Byte, ByVal myOffSet As Int32, ByVal myCount As Int32, ByRef myHash As String, ByRef myHashEx As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "ComputeHash"
        Dim myRes As Byte() = Nothing
        Dim myMd5 As New MD5CryptoServiceProvider
        Try
            myRes = myMd5.ComputeHash(myBuffer, myOffSet, myCount)
            myHash = Convert.ToBase64String(myRes)
            myHashEx = _hexEncoding.ToString(myRes)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        Finally
            myMd5 = Nothing
        End Try
        Return myRes
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myBuffer"></param>
    ''' <param name="myHash"></param>
    ''' <param name="myHashEx"></param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComputeHash(ByRef myBuffer() As Byte, ByRef myHash As String, ByRef myHashEx As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "ComputeHash"
        Dim myRes As Byte() = Nothing
        Dim myMd5 As New MD5CryptoServiceProvider
        Try
            myRes = myMd5.ComputeHash(myBuffer)
            myHash = Convert.ToBase64String(myRes)
            myHashEx = _hexEncoding.ToString(myRes)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        Finally
            myMd5 = Nothing
        End Try
        Return myRes
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myString"></param>
    ''' <param name="myHash"></param>
    ''' <param name="myHashEx"></param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComputeHash(ByRef myString As String, ByRef myHash As String, ByRef myHashEx As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "ComputeHash"
        Dim myBuffer As Byte() = Nothing
        Dim myEnc As New System.Text.ASCIIEncoding
        Dim myRes As Byte() = Nothing
        Try
            myBuffer = myEnc.GetBytes(myString)
            myRes = ComputeHash(myBuffer, myHash, myHashEx, isErr, errMsg)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        Finally
            myEnc = Nothing
        End Try
        Return myRes
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        _hexEncoding = Nothing
    End Sub

    Public Sub New()
        _hexEncoding = New HexEncoding
    End Sub

End Class

''' <summary>
''' Classe per il calcolo della chiave di HASH usando l'algoritmo SHA1
''' </summary>
''' <remarks></remarks>
Public Class SHA1
    Private Const nomeModulo As String = "opencomm.global.SHA1."
    Private _hexEncoding As HexEncoding = Nothing

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myStream"></param>
    ''' <param name="myHash"></param>
    ''' <param name="myHashEx"></param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComputeHash(ByRef myStream As IO.Stream, ByRef myHash As String, ByRef myHashEx As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "ComputeHash"
        Dim myRes As Byte() = Nothing
        Dim mySHA1 As New SHA1CryptoServiceProvider
        Try
            myRes = mySHA1.ComputeHash(myStream)
            myHash = Convert.ToBase64String(myRes)
            myHashEx = _hexEncoding.ToString(myRes)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        Finally
            mySHA1 = Nothing
        End Try
        Return myRes
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myBuffer"></param>
    ''' <param name="myOffSet"></param>
    ''' <param name="myCount"></param>
    ''' <param name="myHash"></param>
    ''' <param name="myHashEx"></param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComputeHash(ByRef myBuffer() As Byte, ByVal myOffSet As Int32, ByVal myCount As Int32, ByRef myHash As String, ByRef myHashEx As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "ComputeHash"
        Dim myRes As Byte() = Nothing
        Dim mySHA1 As New SHA1CryptoServiceProvider
        Try
            myRes = mySHA1.ComputeHash(myBuffer, myOffSet, myCount)
            myHash = Convert.ToBase64String(myRes)
            myHashEx = _hexEncoding.ToString(myRes)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        Finally
            mySHA1 = Nothing
        End Try
        Return myRes
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myBuffer"></param>
    ''' <param name="myHash"></param>
    ''' <param name="myHashEx"></param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComputeHash(ByRef myBuffer() As Byte, ByRef myHash As String, ByRef myHashEx As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "ComputeHash"
        Dim myRes As Byte() = Nothing
        Dim mySHA1 As New SHA1CryptoServiceProvider
        Try
            myRes = mySHA1.ComputeHash(myBuffer)
            myHash = Convert.ToBase64String(myRes)
            myHashEx = _hexEncoding.ToString(myRes)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        Finally
            mySHA1 = Nothing
        End Try
        Return myRes
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myString"></param>
    ''' <param name="myHash"></param>
    ''' <param name="myHashEx"></param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComputeHash(ByRef myString As String, ByRef myHash As String, ByRef myHashEx As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "ComputeHash"
        Dim myBuffer As Byte() = Nothing
        Dim myEnc As New System.Text.ASCIIEncoding
        Dim myRes As Byte() = Nothing
        Try
            myBuffer = myEnc.GetBytes(myString)
            myRes = ComputeHash(myBuffer, myHash, myHashEx, isErr, errMsg)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        Finally
            myEnc = Nothing
        End Try
        Return myRes
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        _hexEncoding = Nothing
    End Sub

    Public Sub New()
        _hexEncoding = New HexEncoding
    End Sub

End Class

''' <summary>
''' Classe di supporto per la gestione di stringhe/array byte
''' </summary>
''' <remarks></remarks>
Public Class HexEncoding
    Private Const nomeModulo As String = "opencomm.global.HexEncoding."

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="hexString"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetByteCount(ByVal hexString As String) As Int32

        Dim numHexChars As Int32 = 0
        Dim c As Char
        Dim i As Int32 = 0
        ' remove all none A-F, 0-9, characters
        For i = 0 To hexString.Length - 1

            c = hexString.ToCharArray()(i)
            If (IsHexDigit(c)) Then
                numHexChars += 1
            End If
        Next i
        '// if odd number of characters, discard last character
        If ((numHexChars Mod 2) <> 0) Then

            numHexChars -= 1
        End If
        Return Convert.ToInt32(numHexChars / 2)  '// 2 characters per byte
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="c"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsHexDigit(ByVal c As Char) As Boolean

        Dim numChar As Int32 = 0
        Dim numA As Int32 = Convert.ToInt32("A")
        Dim num1 As Int32 = Convert.ToInt32("0")
        c = Char.ToUpper(c)
        numChar = Convert.ToInt32(c)
        If (numChar >= numA AndAlso numChar < (numA + 6)) Then
            Return True
        ElseIf (numChar >= num1 AndAlso numChar < (num1 + 10)) Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="hexString"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsHexFormat(ByVal hexString As String) As Boolean

        Dim hexFormat As Boolean = True
        Dim digit As Char
        For Each digit In hexString
            If (Not IsHexDigit(digit)) Then
                hexFormat = False
                Exit For
            End If
        Next
        Return hexFormat
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="hexString"></param>
    ''' <param name="discarded"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBytes(ByVal hexString As String, ByRef discarded As Int32) As Byte()

        discarded = 0
        Dim newString As String = ""
        Dim c As Char
        Dim i As Int32 = 0
        '// remove all none A-F, 0-9, characters
        For i = 0 To hexString.Length - 1

            c = hexString.ToCharArray()(i)
            If (IsHexDigit(c)) Then
                newString += c
            Else
                discarded += 1
            End If
        Next i
        '// if odd number of characters, discard last character
        If (newString.Length Mod 2 <> 0) Then
            discarded += 1
            newString = newString.Substring(0, newString.Length - 1)
        End If

        Dim byteLength As Int32 = Convert.ToInt32(newString.Length / 2)
        Dim bytes(byteLength) As Byte
        Dim hex As String = ""
        Dim j As Int32 = 0
        For i = 0 To bytes.Length - 1
            hex = New String(New Char() {newString.ToCharArray()(j), newString.ToCharArray()(j + 1)})
            bytes(i) = HexToByte(hex)
            j = j + 2
        Next i
        Return bytes
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="hex"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function HexToByte(ByVal hex As String) As Byte

        If (hex.Length > 2 OrElse hex.Length <= 0) Then
            Throw New ArgumentException("hex must be 1 or 2 characters in length")
        End If
        Dim newByte As Byte = Byte.Parse(hex, System.Globalization.NumberStyles.HexNumber)
        Return newByte
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="bytes"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function ToString(ByVal bytes() As Byte) As String
        Dim hexString As String = ""
        Dim i As Int32 = 0
        For i = 0 To bytes.Length - 1
            hexString += bytes(i).ToString("X2")
        Next i
        Return hexString
    End Function

End Class

''' <summary>
''' Classe di supporto per la criptografia TripleDES
''' </summary>
''' <remarks></remarks>
Public Class TripleDes
    Private Const nomeModulo As String = "opencomm.global.TripleDes."

    ' define the triple des provider
    Private _des As TripleDESCryptoServiceProvider = Nothing

    ' define the string handler
    Private _utf8 As UTF8Encoding = Nothing

    ' define the local property arrays
    Private _key() As Byte = Nothing
    Private _iv() As Byte = Nothing

    ''' <summary>
    ''' crea l'oggetto per la criptografia secondo l'algoritmo TripleDes
    ''' </summary>
    ''' <param name="key">Chiave segreta da utilizzare per l'algoritmo simmetrico.</param>
    ''' <param name="iv">Vettore di inizializzazione da utilizzare per l'algoritmo simmetrico.</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal key() As Byte, ByVal iv() As Byte, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "")
        Const nomeFunction As String = nomeModulo & "New"
        Try
            _key = key
            _iv = iv
            _des = New TripleDESCryptoServiceProvider
            _utf8 = New UTF8Encoding
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        _des = Nothing
        _utf8 = Nothing
    End Sub

    ''' <summary>
    ''' cripta la stringa di input secondo le chiavi di inizializzazione
    ''' </summary>
    ''' <param name="input">array di byte rappresentanti la stringa da criptografare</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Encrypt(ByVal input() As Byte, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "Encrypt"
        Dim myRes As Byte() = Nothing
        Try
            myRes = Transform(input, _des.CreateEncryptor(_key, _iv))
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        End Try
        Return myRes
    End Function

    ''' <summary>
    ''' decripta la stringa di input secondo le chiavi di inizializzazione
    ''' </summary>
    ''' <param name="input">array di byte rappresentanti la stringa da decriptografare</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Decrypt(ByVal input() As Byte, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        Const nomeFunction As String = nomeModulo & "Decrypt"
        Dim myRes As Byte() = Nothing
        Try
            myRes = Transform(input, _des.CreateDecryptor(_key, _iv))
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        End Try
        Return myRes
    End Function

    ''' <summary>
    ''' cripta la stringa di input secondo le chiavi di inizializzazione
    ''' </summary>
    ''' <param name="text">stringa da criptografare</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Encrypt(ByVal text As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As String
        Const nomeFunction As String = nomeModulo & "Encrypt"
        Dim myRes As String = ""
        Dim input() As Byte = Nothing
        Dim output() As Byte = Nothing
        Try
            input = _utf8.GetBytes(text)
            output = Transform(input, _des.CreateEncryptor(_key, _iv))
            myRes = Convert.ToBase64String(output)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        End Try
        Return myRes
    End Function

    ''' <summary>
    ''' decripta la stringa di input secondo le chiavi di inizializzazione
    ''' </summary>
    ''' <param name="text">stringa da decriptografare</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Decrypt(ByVal text As String, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As String
        Const nomeFunction As String = nomeModulo & "Decrypt"
        Dim myRes As String = ""
        Dim input() As Byte = Nothing
        Dim output() As Byte = Nothing
        Try
            input = _utf8.GetBytes(text)
            output = Transform(input, _des.CreateDecryptor(_key, _iv))
            myRes = Convert.ToBase64String(output)
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        End Try
        Return myRes

    End Function

    ''' <summary>
    ''' effettua la trasformazione della stringa secondo l'algoritimo TripleDes
    ''' </summary>
    ''' <param name="input"></param>
    ''' <param name="CryptoTransform"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function Transform(ByVal input() As Byte, ByVal CryptoTransform As ICryptoTransform, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Byte()
        ' create the necessary streams
        Const nomeFunction As String = nomeModulo & "Transform"
        Dim result() As Byte = Nothing
        Dim memStream As MemoryStream = Nothing
        Dim cryptStream As CryptoStream = Nothing
        Try
            memStream = New MemoryStream
            cryptStream = New CryptoStream(memStream, CryptoTransform, CryptoStreamMode.Write)

            ' transform the bytes as requested
            cryptStream.Write(input, 0, input.Length)
            cryptStream.FlushFinalBlock()

            ' Read the memory stream and convert it back into byte array
            memStream.Position = 0
            ReDim result(CType(memStream.Length - 1, System.Int32))
            memStream.Read(result, 0, CType(result.Length, System.Int32))

        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex, errMsg)
        Finally
            If Not memStream Is Nothing Then memStream.Close()
            If Not cryptStream Is Nothing Then cryptStream.Close()
            memStream = Nothing
            cryptStream = Nothing
        End Try
        Return result
    End Function

End Class