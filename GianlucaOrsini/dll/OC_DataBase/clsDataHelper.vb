#Region " Option Support "

Option Explicit On
Option Strict On

#End Region

#Region " Imports Support "

Imports opencomm.global
Imports opencomm.global.utility
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

#End Region

''' <summary>
''' Classe di supporto per l'accesso e la gestione dei database
''' </summary>
''' <remarks></remarks>
Public Class DataHelper

    Const nomeModulo As String = "opencomm.database.DataHelper."

    ''' <summary>
    ''' Enumerazione dei codici di errore che possono essere restituiti dall'esecuzuine
    ''' di un command
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum eReturnError As Integer
        eErroreGenerico = -2
        eNoDbConncetion = -1
        eOk = 0
    End Enum

    ''' <summary>
    ''' Metodo privato che restituisce un oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database
    ''' connesso
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns>Oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database da utilizzare per eseguire i comandi</returns>
    ''' <remarks></remarks>
    Private Shared Function GivMeDb(Optional ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database = Nothing, Optional ByVal cnnStrName As String = "", Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As Microsoft.Practices.EnterpriseLibrary.Data.Database
        Const nomeFunction As String = nomeModulo & "GivMeDb"
        Try
            If myDb Is Nothing Then
                If cnnStrName = "" Then
                    Return DatabaseFactory.CreateDatabase()
                Else
                    Return DatabaseFactory.CreateDatabase(cnnStrName)
                End If
            Else
                Return myDb
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Esegue un comando sul database e restituisce il numero di righe oppure un codice di errore
    ''' </summary>
    ''' <param name="myDb">Database su cui eseguire il comando, se nothing viene aperta una connesione al volo</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="storedProcedureName">Nome della stored procedure da eseguire</param>
    ''' <param name="parameterValues">Parametri da passare all'esecuzione della stored procedure</param>
    ''' <returns>Numero di record restituiti dal comando o codice di errore</returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDbCommand(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Integer
        Const nomeFunction As String = nomeModulo & "ExecuteDbCommand"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteNonQuery(storedProcedureName, parameterValues)
            Else
                Return eReturnError.eNoDbConncetion
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return eReturnError.eErroreGenerico
        End Try
    End Function

    ''' <summary>
    ''' Esegue un comando sul database e restituisce il numero di righe oppure un codice di errore
    ''' </summary>
    ''' <param name="myDb">Database su cui eseguire il comando, se nothing viene aperta una connesione al volo</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns>Numero di record restituiti dal comando o codice di errore</returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDbCommand(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Integer
        Const nomeFunction As String = nomeModulo & "ExecuteDbCommand"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteNonQuery(commandType, commandText)
            Else
                Return eReturnError.eNoDbConncetion
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return eReturnError.eErroreGenerico
        End Try
    End Function

    ''' <summary>
    ''' Esegue un comando sul database e restituisce il numero di righe oppure un codice di errore
    ''' </summary>
    ''' <param name="myDb">Database su cui eseguire il comando, se nothing viene aperta una connesione al volo</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <returns>Numero di record restituiti dal comando o codice di errore</returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDbCommand(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand) As Integer
        Const nomeFunction As String = nomeModulo & "ExecuteDbCommand"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteNonQuery(command)
            Else
                Return eReturnError.eNoDbConncetion
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return eReturnError.eErroreGenerico
        End Try
    End Function

    ''' <summary>
    ''' Esegue un comando sul database e restituisce il numero di righe oppure un codice di errore
    ''' </summary>
    ''' <param name="myDb">Database su cui eseguire il comando, se nothing viene aperta una connesione al volo</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <param name="transaction"></param>
    ''' <returns>Numero di record restituiti dal comando o codice di errore</returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDbCommand(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand, ByVal transaction As System.Data.Common.DbTransaction) As Integer
        Const nomeFunction As String = nomeModulo & "ExecuteDbCommand"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteNonQuery(command, transaction)
            Else
                Return eReturnError.eNoDbConncetion
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return eReturnError.eErroreGenerico
        End Try
    End Function

    ''' <summary>
    ''' Esegue un comando sul database e restituisce il numero di righe oppure un codice di errore
    ''' </summary>
    ''' <param name="myDb">Database su cui eseguire il comando, se nothing viene aperta una connesione al volo</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns>Numero di record restituiti dal comando o codice di errore</returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDbCommand(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Integer
        Const nomeFunction As String = nomeModulo & "ExecuteDbCommand"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteNonQuery(transaction, storedProcedureName, parameterValues)
            Else
                Return eReturnError.eNoDbConncetion
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return eReturnError.eErroreGenerico
        End Try
    End Function

    ''' <summary>
    ''' Esegue un comando sul database e restituisce il numero di righe oppure un codice di errore
    ''' </summary>
    ''' <param name="myDb">Database su cui eseguire il comando, se nothing viene aperta una connesione al volo</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns>Numero di record restituiti dal comando o codice di errore</returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDbCommand(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Integer
        Const nomeFunction As String = nomeModulo & "ExecuteDbCommand"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteNonQuery(transaction, commandType, commandText)
            Else
                Return eReturnError.eNoDbConncetion
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return eReturnError.eErroreGenerico
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataTable(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Data.DataTable
        Const nomeFunction As String = nomeModulo & "ExecuteDataTable"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try

            For Each myObj As Object In parameterValues
                If myObj Is Nothing Then myObj = DBNull.Value
            Next

            If Not db Is Nothing Then
                Dim myDs As DataSet = ExecuteDataSet(myDb, cnnStrName, isErr, errMsg, storedProcedureName, parameterValues)
                If Not myDs Is Nothing AndAlso myDs.Tables.Count > 0 Then
                    Return myDs.Tables(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataTable(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Data.DataTable
        Const nomeFunction As String = nomeModulo & "ExecuteDataTable"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Dim myDs As DataSet = ExecuteDataSet(myDb, cnnStrName, isErr, errMsg, commandType, commandText)
                If Not myDs Is Nothing AndAlso myDs.Tables.Count > 0 Then
                    Return myDs.Tables(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataTable(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand) As Data.DataTable
        Const nomeFunction As String = nomeModulo & "ExecuteDataTable"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Dim myDs As DataSet = ExecuteDataSet(myDb, cnnStrName, isErr, errMsg, command)
                If Not myDs Is Nothing AndAlso myDs.Tables.Count > 0 Then
                    Return myDs.Tables(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataTable(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand, ByVal transaction As System.Data.Common.DbTransaction) As Data.DataTable
        Const nomeFunction As String = nomeModulo & "ExecuteDataTable"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try

            If Not db Is Nothing Then
                Dim myDs As DataSet = ExecuteDataSet(myDb, cnnStrName, isErr, errMsg, command, transaction)
                If Not myDs Is Nothing AndAlso myDs.Tables.Count > 0 Then
                    Return myDs.Tables(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataTable(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Data.DataTable
        Const nomeFunction As String = nomeModulo & "ExecuteDataTable"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            For Each myObj As Object In parameterValues
                If myObj Is Nothing Then myObj = DBNull.Value
            Next

            If Not db Is Nothing Then
                Dim myDs As DataSet = ExecuteDataSet(myDb, cnnStrName, isErr, errMsg, transaction, storedProcedureName, parameterValues)
                If Not myDs Is Nothing AndAlso myDs.Tables.Count > 0 Then
                    Return myDs.Tables(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataTable(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Data.DataTable
        Const nomeFunction As String = nomeModulo & "ExecuteDataTable"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Dim myDs As DataSet = ExecuteDataSet(myDb, cnnStrName, isErr, errMsg, transaction, commandType, commandText)
                If Not myDs Is Nothing AndAlso myDs.Tables.Count > 0 Then
                    Return myDs.Tables(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataSet(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Data.DataSet
        Const nomeFunction As String = nomeModulo & "ExecuteDataSet"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try

            For Each myObj As Object In parameterValues
                If myObj Is Nothing Then myObj = DBNull.Value
            Next

            If Not db Is Nothing Then
                Return db.ExecuteDataSet(storedProcedureName, parameterValues)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataSet(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Data.DataSet
        Const nomeFunction As String = nomeModulo & "ExecuteDataSet"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteDataSet(commandType, commandText)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataSet(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand) As Data.DataSet
        Const nomeFunction As String = nomeModulo & "ExecuteDataSet"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteDataSet(command)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <param name="transaction"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataSet(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand, ByVal transaction As System.Data.Common.DbTransaction) As Data.DataSet
        Const nomeFunction As String = nomeModulo & "ExecuteDataSet"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try

            If Not db Is Nothing Then
                Return db.ExecuteDataSet(command, transaction)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataSet(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Data.DataSet
        Const nomeFunction As String = nomeModulo & "ExecuteDataSet"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            For Each myObj As Object In parameterValues
                If myObj Is Nothing Then myObj = DBNull.Value
            Next

            If Not db Is Nothing Then
                Return db.ExecuteDataSet(transaction, storedProcedureName, parameterValues)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataSet(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Data.DataSet
        Const nomeFunction As String = nomeModulo & "ExecuteDataSet"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteDataSet(transaction, commandType, commandText)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataRow(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Data.DataRow
        Const nomeFunction As String = nomeModulo & "ExecuteDataRow"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try

            For Each myObj As Object In parameterValues
                If myObj Is Nothing Then myObj = DBNull.Value
            Next

            If Not db Is Nothing Then
                Dim myDt As DataTable = ExecuteDataTable(myDb, cnnStrName, isErr, errMsg, storedProcedureName, parameterValues)
                If myDt.Rows.Count > 0 Then
                    Return myDt.Rows(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataRow(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Data.DataRow
        Const nomeFunction As String = nomeModulo & "ExecuteDataRow"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Dim myDt As DataTable = ExecuteDataTable(myDb, cnnStrName, isErr, errMsg, commandType, commandText)
                If myDt.Rows.Count > 0 Then
                    Return myDt.Rows(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataRow(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand) As Data.DataRow
        Const nomeFunction As String = nomeModulo & "ExecuteDataRow"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Dim myDt As DataTable = ExecuteDataTable(myDb, cnnStrName, isErr, errMsg, command)
                If myDt.Rows.Count > 0 Then
                    Return myDt.Rows(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <param name="transaction"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataRow(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand, ByVal transaction As System.Data.Common.DbTransaction) As Data.DataRow
        Const nomeFunction As String = nomeModulo & "ExecuteDataRow"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Dim myDt As DataTable = ExecuteDataTable(myDb, cnnStrName, isErr, errMsg, command, transaction)
                If myDt.Rows.Count > 0 Then
                    Return myDt.Rows(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataRow(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Data.DataRow
        Const nomeFunction As String = nomeModulo & "ExecuteDataRow"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            For Each myObj As Object In parameterValues
                If myObj Is Nothing Then myObj = DBNull.Value
            Next
            If Not db Is Nothing Then
                Dim myDt As DataTable = ExecuteDataTable(myDb, cnnStrName, isErr, errMsg, transaction, storedProcedureName, parameterValues)
                If myDt.Rows.Count > 0 Then
                    Return myDt.Rows(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteDataRow(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Data.DataRow
        Const nomeFunction As String = nomeModulo & "ExecuteDataRow"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Dim myDt As DataTable = ExecuteDataTable(myDb, cnnStrName, isErr, errMsg, transaction, commandType, commandText)
                If myDt.Rows.Count > 0 Then
                    Return myDt.Rows(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteScalar(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Object
        Const nomeFunction As String = nomeModulo & "ExecuteScalar"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteScalar(storedProcedureName, parameterValues)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteScalar(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Object
        Const nomeFunction As String = nomeModulo & "ExecuteScalar"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteScalar(commandType, commandText)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteScalar(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand) As Object
        Const nomeFunction As String = nomeModulo & "ExecuteScalar"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteScalar(command)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <param name="transaction"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteScalar(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand, ByVal transaction As System.Data.Common.DbTransaction) As Object
        Const nomeFunction As String = nomeModulo & "ExecuteScalar"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteScalar(command, transaction)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteScalar(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As Object
        Const nomeFunction As String = nomeModulo & "ExecuteScalar"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteScalar(transaction, storedProcedureName, parameterValues)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteScalar(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As Object
        Const nomeFunction As String = nomeModulo & "ExecuteScalar"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteScalar(transaction, commandType, commandText)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteReader(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As System.Data.IDataReader
        Const nomeFunction As String = nomeModulo & "ExecuteReader"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteReader(storedProcedureName, parameterValues)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteReader(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As System.Data.IDataReader
        Const nomeFunction As String = nomeModulo & "ExecuteReader"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteReader(commandType, commandText)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteReader(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand) As System.Data.IDataReader
        Const nomeFunction As String = nomeModulo & "ExecuteReader"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteReader(command)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="command"></param>
    ''' <param name="transaction"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteReader(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal command As System.Data.Common.DbCommand, ByVal transaction As System.Data.Common.DbTransaction) As System.Data.IDataReader
        Const nomeFunction As String = nomeModulo & "ExecuteReader"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteReader(command, transaction)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="storedProcedureName"></param>
    ''' <param name="parameterValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteReader(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal storedProcedureName As String, ByVal ParamArray parameterValues() As Object) As System.Data.IDataReader
        Const nomeFunction As String = nomeModulo & "ExecuteReader"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteReader(transaction, storedProcedureName, parameterValues)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="myDb">Eventuale oggetto Microsoft.Practices.EnterpriseLibrary.Data.Database gia connesso</param>
    ''' <param name="cnnStrName">Chiave della connectione string da utilizzare definita in web.config, se vuoto viene utilizzata la connessione di default</param>
    ''' <param name="isErr"></param>
    ''' <param name="errMsg"></param>
    ''' <param name="transaction"></param>
    ''' <param name="commandType"></param>
    ''' <param name="commandText"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ExecuteReader(ByVal myDb As Microsoft.Practices.EnterpriseLibrary.Data.Database, ByVal cnnStrName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal transaction As System.Data.Common.DbTransaction, ByVal commandType As System.Data.CommandType, ByVal commandText As String) As System.Data.IDataReader
        Const nomeFunction As String = nomeModulo & "ExecuteReader"
        Dim db As Microsoft.Practices.EnterpriseLibrary.Data.Database = GivMeDb(myDb, cnnStrName)
        Try
            If Not db Is Nothing Then
                Return db.ExecuteReader(transaction, commandType, commandText)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Esegue un raggruppamento dei dati sul campo specificato.
    ''' </summary>
    ''' <param name="sourceTable">Tabella origine da cui eseguire il raggruppamento.</param>
    ''' <param name="groupByFieldName">Nome della colonna sulla tabella origine su cui fare il raggruppamento.</param>
    ''' <param name="headerFieldNames">Nome delle colonne da riportare sulla tabella padre.</param>
    ''' <returns></returns>
    ''' <remarks>Partendo dalla tabella passata come parametro, crea una tabella sul dataset 
    ''' in relazione padre-figlio con quella passata e ritorna la tabella padre. 
    ''' Usato per creare griglie gerarchiche. </remarks>
    Public Shared Function MakeGroupBy(ByVal sourceTable As DataTable, ByVal groupByFieldName As String, ByRef isErr As Boolean, ByRef errMsg As String, ByVal ParamArray headerFieldNames() As String) As DataTable
        Const nomeFunction As String = nomeModulo & "MakeGroupBy"
        Try
            If IsNothing(sourceTable) Then Throw New ArgumentNullException("sourceTable")
            If String.IsNullOrEmpty(groupByFieldName) Then Throw New ArgumentNullException("groupByFieldName")
            If Not sourceTable.Columns.Contains(groupByFieldName) Then Throw New ArgumentException("La colonna " & groupByFieldName & " non esiste sulla tabella di raggruppamento!")
            If Not IsNothing(headerFieldNames) AndAlso headerFieldNames.Length > 0 Then
                For i As Integer = 0 To headerFieldNames.Length - 1
                    If Not sourceTable.Columns.Contains(headerFieldNames(i)) Then _
                        Throw New ArgumentException("La colonna " & headerFieldNames(i) & " non esiste sulla tabella di raggruppamento!")
                Next
            End If

            Dim ds As DataSet = sourceTable.DataSet
            If IsNothing(ds) Then
                ds = New DataSet()
                ds.Tables.Add(sourceTable)
            End If

            Dim dtPadre As New DataTable("Padre_" & sourceTable.TableName)
            If ds.Tables.Contains(dtPadre.TableName) Then
                Dim i As Integer = 0
                While ds.Tables.Contains(dtPadre.TableName)
                    i += 1
                    dtPadre.TableName = "Padre_" & sourceTable.TableName & i
                End While
            End If
            ds.Tables.Add(dtPadre)

            Dim dcOrigCol As DataColumn = sourceTable.Columns(groupByFieldName)
            CopyColumnDefinition(dcOrigCol, dtPadre)

            If Not IsNothing(headerFieldNames) Then
                ' copia le colonne header sulla padre
                For i As Integer = 0 To headerFieldNames.Length - 1
                    Dim sourceColumn As DataColumn = sourceTable.Columns(headerFieldNames(i))

                    CopyColumnDefinition(sourceColumn, dtPadre)
                Next
            End If

            Dim prevAllowNull As Boolean = dtPadre.Columns(0).AllowDBNull
            dtPadre.PrimaryKey = New DataColumn() {dtPadre.Columns(0)}
            dtPadre.Columns(0).AllowDBNull = prevAllowNull

            dtPadre.Columns(0).AllowDBNull = True
            ' applica l'ordinamento alla tabella originale per trovare velocemente i gruppi
            Dim sortedTable As New DataView(sourceTable)
            sortedTable.Sort = groupByFieldName

            ' estrae le chiavi uniche e le salva sulla tabella padre
            Dim prevValue As Object = Nothing
            For Each row As DataRowView In sortedTable
                ' crea il padre se diverso dal precedente
                Dim currentValue As Object = row(dcOrigCol.Ordinal)
                If IsNothing(prevValue) OrElse _
                    (IsDBNull(prevValue) AndAlso Not IsDBNull(currentValue)) OrElse _
                    (IsDBNull(currentValue) AndAlso Not IsDBNull(prevValue)) OrElse _
                    (Not IsDBNull(currentValue) AndAlso Not IsDBNull(prevValue) AndAlso Not (currentValue Is prevValue)) Then

                    Dim newPadreRow As DataRow = dtPadre.NewRow()
                    newPadreRow(0) = currentValue
                    For i As Integer = 1 To dtPadre.Columns.Count - 1
                        newPadreRow(i) = row(dtPadre.Columns(i).ColumnName)
                    Next
                    dtPadre.Rows.Add(newPadreRow)
                    prevValue = currentValue
                End If
            Next

            ' crea la relazione padre-figlio
            Dim rel As New DataRelation("", dtPadre.Columns(0), dcOrigCol)
            dtPadre.ChildRelations.Add(rel)
            Return dtPadre
        Catch ex As Exception
            isErr = True
            errMsg = getErrore(nomeFunction, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Duplica la definizione di colonna specificata e la aggiunge alla tabella destinazione.
    ''' </summary>
    ''' <param name="sourceColumn">Colonna da duplicare.</param>
    ''' <param name="destTable">Tabella di destinazione.</param>
    ''' <remarks>Ritorna la nuova colonna aggiunta alla tabella.</remarks>
    Public Shared Function CopyColumnDefinition(ByVal sourceColumn As DataColumn, ByVal destTable As DataTable, Optional ByRef isErr As Boolean = False, Optional ByRef errMsg As String = "") As DataColumn
        Const nomeFunction As String = nomeModulo & "CopyColumnDefinition"
        Dim destColumn As New DataColumn()
        Try
            destColumn.ColumnName = sourceColumn.ColumnName
            destColumn.AllowDBNull = sourceColumn.AllowDBNull
            destColumn.AutoIncrement = sourceColumn.AutoIncrement
            destColumn.AutoIncrementSeed = sourceColumn.AutoIncrementSeed
            destColumn.AutoIncrementStep = sourceColumn.AutoIncrementStep
            destColumn.Caption = sourceColumn.Caption
            destColumn.DataType = sourceColumn.DataType
            destColumn.DefaultValue = sourceColumn.DefaultValue
            destColumn.Expression = sourceColumn.Expression
            destColumn.MaxLength = sourceColumn.MaxLength
            destColumn.ReadOnly = sourceColumn.ReadOnly
            destColumn.Unique = sourceColumn.Unique
            destTable.Columns.Add(destColumn)
        Catch ex As Exception
            destColumn = Nothing
            errMsg = getErrore(nomeFunction, ex, errMsg)
        End Try
        Return destColumn
    End Function

End Class
