﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="database_add.aspx.vb" Inherits="GianlucaOrsini.database_add" MasterPageFile="~/Site1.Master"  %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="script/database_add.js"></script>

    <div>
      <br />
        <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Size="Large" Text="INSERISCI NOMINATIVI IN RUBRICA"></asp:Label>
      <br />
      <br />
    </div>
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Nome"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtNome" runat="server" Width="178px"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label2" runat="server" Text="Cognome"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtCognome" runat="server" Width="173px"></asp:TextBox>
        <br />
        <asp:Label ID="Label3" runat="server" Text="email"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtEmail" runat="server" Width="177px"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label4" runat="server" Text="Cellulare"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtCellulare" runat="server" Width="176px"></asp:TextBox>
        <br />
        <asp:Label ID="Label5" runat="server" Text="indirizzo"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtIndirizzo" runat="server" Width="523px"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Inserisci in Anagrafica" OnClientClick="return checkAddNominativo();" />
    
    &nbsp;
        <asp:Button ID="Button3" runat="server" Text="Annulla" />
        <br />
        <br />
        <br />
        <asp:Label ID="lblresult" runat="server" Text="---"></asp:Label>
        <br />
        <asp:Button ID="Button2" runat="server" Text="Visualizza Anagrafica" Visible="False" />
    
    </div>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentMenu">
    <link href="cssMenu.css" rel="stylesheet" type="text/css" />
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>
    <asp:sitemapdatasource id="SiteMapDataSource1" runat="server" sitemapprovider="" showstartingnode="false" startingnodeoffset="0" />
        <div id="Page">
           <div id="Container">
            <asp:menu id="Menu1" runat="server" datasourceid="SiteMapDataSource1" orientation="Horizontal" width="95%" staticdisplaylevels="1" Height="30px">
               <dynamicmenustyle />
            </asp:menu>
            <div class="Breadcrumbs">
                <asp:sitemappath id="SiteMapPath1" runat="server"></asp:sitemappath>
            </div>
           </div>
        </div>
</asp:Content>
    
    
