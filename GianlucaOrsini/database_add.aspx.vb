﻿Imports System.Data.SqlClient

Public Class database_add
    Inherits System.Web.UI.Page

    Dim username As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("username")
        Button2.Visible = False
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'using (SqlConnection connection = new SqlConnection(connectionString)) 
        '    {
        'connection.Open(); 
        'using (SqlCommand command = connection.CreateCommand()) 
        '{ 
        '    command.CommandText = "INSERT INTO klant(klant_id,naam,voornaam) VALUES(@param1,@param2,@param3)";  

        '    command.Parameters.AddWithValue("@param1", klantId));  
        '    command.Parameters.AddWithValue("@param2", klantNaam));  
        '    command.Parameters.AddWithValue("@param3", klantVoornaam));  

        '    command.ExecuteNonQuery(); 
        '} 

        Dim queryString As String = "INSERT INTO customers(name,surname,email,cell,address,father) VALUES(@NOME,@COGNOME,@EMAIL,@CELL,@INDIRIZZO,@FATHER)"

        Using mySqlConnection As New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")
            Dim command As New SqlCommand(queryString, mySqlConnection)

            mySqlConnection.Open()

            command.Parameters.AddWithValue("@NOME", txtNome.Text)
            command.Parameters.AddWithValue("@COGNOME", txtCognome.Text)
            command.Parameters.AddWithValue("@EMAIL", txtEmail.Text)
            command.Parameters.AddWithValue("@CELL", txtCellulare.Text)
            command.Parameters.AddWithValue("@INDIRIZZO", txtIndirizzo.Text)
            command.Parameters.AddWithValue("@FATHER", username)
            Try
                command.ExecuteNonQuery()
                lblresult.Text = "Nominativo inserito correttamente"
                Button2.Visible = True
            Catch ex As Exception
                lblresult.Text = ex.Message
            End Try


        End Using


    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Response.Redirect("database.aspx?username=" + username)
    End Sub

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Response.Redirect("database.aspx?username=" + username)
    End Sub
End Class