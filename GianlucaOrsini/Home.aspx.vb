﻿Imports System.Data.SqlClient

Public Class Home
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim DBConnection As New MySqlConnection With System.Configuration.ConfigurationManager.ConnectionStrings("My_ConnectionString").ConnectionString)

        'Dim mySqlConnection As New MySql.Data.MySqlClient.MySqlConnection("your_connection_string")

        'Dim xmlFilePath As String = System.Configuration.ConfigurationManager.AppSettings("your_connection_string")

        'Dim mySqlConnection As MySql.Data.MySqlClient.MySqlConnection = New MySql.Data.MySqlClient.MySqlConnection
        'mySqlConnection.ConnectionString = xmlFilePath

        'Dim mySqlConnection As SqlConnection = New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")

       
        
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            'mySqlConnection.Open()
            Dim queryString As String = "SELECT nickname,password from utenti WHERE nickname like @ID ;"


            Using mySqlConnection As New SqlConnection("Data Source=(LocalDB)\v11.0;AttachDbFileName=|DataDirectory|\gianluca.mdf;Integrated Security=True;MultipleActiveResultSets=True")
                Dim command As New SqlCommand(queryString, mySqlConnection)
                command.Parameters.Add("@ID", SqlDbType.Text)
                command.Parameters("@ID").Value = TxtNickName.Text

                mySqlConnection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader()
                reader.Read()
                If reader.HasRows Then
                    If String.Compare(TxtPassword.Text, reader(1).ToString, True) = 1 Then
                        Label3.Text = "Password Sbagliata"
                        Label3.Visible = True
                    Else
                        Label3.Text = "Password Corretta .. redirect in corso"
                        Label3.Visible = True
                        Response.Redirect("Dashboard.aspx?username=" + TxtNickName.Text)
                    End If
                Else
                    Label3.Text = "nessun username corrispondente"
                    Label3.Visible = True
                End If
                reader.Close()

            End Using
        Catch ex As Exception
            Label3.Text = ex.Message
        End Try
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Response.Redirect("Mappa.aspx?username=" + TxtNickName.Text)
    End Sub
End Class