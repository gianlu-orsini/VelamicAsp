﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Mappa.aspx.vb" Inherits="GianlucaOrsini.Mappa" MasterPageFile="~/Site1.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 50%;width:70%; }
      body { height: 100%; margin: 0; padding: 0 }
      #map-canvas { height: 100% }
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6O4fRgPeQBpzSQP3uxrTCsq8AtDofKFA&sensor=false">
    </script>
    <script type="text/javascript">
        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng(-34.397, 150.644),
                zoom: 2,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map-canvas"),
                mapOptions);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <div id="map-canvas" />
      <div>
          <hr />
      </div>

</asp:Content>
