﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Campagna_elenco.aspx.vb" Inherits="GianlucaOrsini.Campagna_elenco" MasterPageFile="~/Site1.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <div>
     <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="ELENCO CAMPAGNE:"></asp:Label>
        <br />
        <br />
        <asp:Label ID="lblNumeroContatti" runat="server" Text="Label"></asp:Label>
        <br />
        <br />
 </div>
    <div>  
      <asp:GridView ID="GridView1" runat="server" 
            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" AllowPaging="True" PageSize="20" Width="804px">  
            <Columns>  
                <asp:BoundField DataField="name" HeaderText="Nome Campagna" ReadOnly="True"  ItemStyle-Width="150" SortExpression="name" >  
                <FooterStyle BackColor="#FF0066" />
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="type" HeaderText="Tipo"   ItemStyle-Width="150" SortExpression="type" >  
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="datacreazione" HeaderText="Data Creazione"   ItemStyle-Width="150" SortExpression="datacreazione" >  
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="numeroInvii" HeaderText="Numero Invii"   ItemStyle-Width="150" SortExpression="numeroInvii" >  
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="dataUltimoInvio" HeaderText="Data Ultimo Invio"   ItemStyle-Width="150" SortExpression="dataUltimoInvio" >   
<ItemStyle Width="150px"></ItemStyle>
                </asp:BoundField>
                <asp:CommandField ButtonType="Button" SelectText="Dettaglio" ShowSelectButton="True" />
                <asp:CommandField ButtonType="Button" ShowEditButton="True" />
                <asp:CommandField ButtonType="Button" ShowDeleteButton="True" ShowHeader="True" />
            </Columns>  
            <PagerSettings PageButtonCount="30" />
        </asp:GridView>    
    </div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div>
        <asp:Button ID="Button1" runat="server" Text="Crea Nuova Campagna" />
    &nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" Text="DashBoard" />
    </div>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentMenu">
    <link href="cssMenu.css" rel="stylesheet" type="text/css" />
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>
    <asp:sitemapdatasource id="SiteMapDataSource1" runat="server" sitemapprovider="" showstartingnode="false" startingnodeoffset="0" />
        <div id="Page">
           <div id="Container">
            <asp:menu id="Menu1" runat="server" datasourceid="SiteMapDataSource1" orientation="Horizontal" width="95%" staticdisplaylevels="1" Height="30px">
               <dynamicmenustyle />
            </asp:menu>
            <div class="Breadcrumbs">
                <asp:sitemappath id="SiteMapPath1" runat="server"></asp:sitemappath>
            </div>
           </div>
        </div>
</asp:Content>
