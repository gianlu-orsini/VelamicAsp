﻿Public Class Dashboard
    Inherits System.Web.UI.Page

    Dim username As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("username")
        lblWelcome.Text = "Benvenuto " + username + " nella nostra Dashboard"

    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Response.Redirect("database.aspx?username=" + username)
    End Sub

    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Response.Redirect("database_add.aspx?username=" + username)
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("database_import.aspx?username=" + username)
    End Sub

    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Response.Redirect("creaCampagna.aspx?username=" + username)
    End Sub

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Response.Redirect("Campagna_elenco.aspx?username=" + username)
    End Sub
End Class